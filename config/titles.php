<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Titles for routes names
    |--------------------------------------------------------------------------
    |
    | Set Titles for each admin routes names
    */

    'admin' => 'dashboard',
    'users' => [
        'index' => 'usersGestion',
        'edit' => 'userEdit',
    ],
    'contacts' => [
        'index' => 'contactsGestion',
    ],
    'posts' => [
        'index' => 'postsGestion',
        'edit' => 'postEdit',
        'create' => 'postCreate',
        'show' => 'postShow',
    ],
    'notifications' => [
        'index' => 'notificationsGestion',
    ],
    'comments' => [
        'index' => 'commentsGestion',
    ],
    'medias' => [
        'index' => 'mediasGestion',
    ],
    'settings' => [
        'edit' => 'settings',
    ],
    'categories' => [
        'index' => 'categoriesGestion',
        'create' => 'categoryCreate',
        'edit' => 'categoryEdit',
    ],
    'sacerdotes' => [
        'index' => 'sacerdotesGestion',
        'create' => 'sacerdotesCreate',
        'edit' => 'sacerdotesEdit',
    ],
    'eucaristias' => [
        'index' => 'eucaristiasGestion',
        'create' => 'eucaristiasCreate',
        'edit' => 'eucaristiasEdit',
    ],
    'bautizos' => [
        'index' => 'bautizosGestion',
        'create' => 'bautizosCreate',
        'edit' => 'bautizosEdit',
    ],
    'confirms' => [
        'index' => 'confirmacionesGestion',
        'create' => 'confirmacionesCreate',
        'edit' => 'confirmacionesEdit',
    ],
    'matrimonios' => [
        'index' => 'matrimoniosGestion',
        'create' => 'matrimoniosCreate',
        'edit' => 'matrimoniosEdit',
    ],
    'defuncs' => [
        'index' => 'defuncionesGestion',
        'create' => 'defuncionesCreate',
        'edit' => 'defuncionesEdit',
    ],

];