<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Names, icons and urls for urls segments
    |--------------------------------------------------------------------------
    |
    | Set names, icons and urls for each admin url segment
    */

    'admin' => [
        'name' => 'dashboard',
        'icon' => 'dashboard',
        'url' => '/admin',
    ],
    'posts' =>
    [
        'name' => 'posts',
        'icon' => 'file-text',
        'url' => '/admin/posts',
    ],
    'users' =>
    [
        'name' => 'users',
        'icon' => 'user',
        'url' => '/admin/users',
    ],
    'edit' =>
    [
        'name' => 'edition',
        'icon' => 'edit',
    ],
    'create' =>
    [
        'name' => 'creation',
        'icon' => 'edit'
    ],
    'contacts' =>
    [
        'name' => 'contacts',
        'icon' => 'envelope',
        'url' => '#',
    ],
    'notifications' =>
    [
        'name' => 'notifications',
        'icon' => 'bell',
        'url' => '#',
    ],
    'comments' =>
    [
        'name' => 'comments',
        'icon' => 'comment',
        'url' => '#',
    ],
    'medias' =>
    [
        'name' => 'medias',
        'icon' => 'image',
        'url' => '#',
    ],
    'settings' =>
    [
        'name' => 'settings',
        'icon' => 'cog',
        'url' => '#',
    ],
    'categories' =>
    [
        'name' => 'categories',
        'icon' => 'list',
        'url' => '/admin/categories',
    ],
    'sacerdotes' =>
    [
        'name' => 'sacerdotes',
        'icon' => 'list',
        'url' => '/admin/sacerdotes',
    ],
    'eucaristias' =>
    [
        'name' => 'eucaristias',
        'icon' => 'folder',
        'url' => '/admin/eucaristias',
    ],
    'bautizos' =>
    [
        'name' => 'bautizos',
        'icon' => 'folder',
        'url' => '/admin/bautizos',
    ],
    'confirms' =>
    [
        'name' => 'confirmaciones',
        'icon' => 'folder',
        'url' => '/admin/confirms',
    ],
    'matrimonios' =>
    [
        'name' => 'matrimonios',
        'icon' => 'folder',
        'url' => '/admin/matrimonios',
    ],
    'defuncs' =>
    [
        'name' => 'defunciones',
        'icon' => 'folder',
        'url' => '/admin/defuncs',
    ],

];