<?php

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\Comment;
use App\Models\User;
use App\Models\Contact;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Users
        User::create(
            [
                'name' => 'Administrador',
                'email' => 'catedraldecoro@gmail.com',
                'password' => bcrypt('@dminBa$ilic@'),
                'role' => 'admin',
                'valid' => true,
                'confirmed' => true,
                'remember_token' => str_random(10),
            ]
        );

        // Uncheck new for these users
        foreach(User::all() as $user) {
            $user->ingoing->delete();
        }

        $nbrUsers = 5;

        // Categories
        DB::table('categories')->insert([
            [
                'title' => 'Matrimonio',
                'slug' => 'matrimonio'
            ],
            [
                'title' => 'Confirmacion',
                'slug' => 'confirmacion'
            ],
            [
                'title' => 'Bautizo',
                'slug' => 'bautizo'
            ],
            [
                'title' => 'Eucaristía',
                'slug' => 'eucaristia'
            ],
        ]);

        // Sacerdotes
        DB::table('sacerdotes')->insert([
            [
                'nombre' => 'Robert Ramón Medina Lázaro',
                'titulo' => 'Monseñor'
            ],
        ]);

        $nbrCategories = 10;

        // Posts
        Post::create([
            'title' => 'Matrimonio',
            'slug' => 'matrimonio',
            'user_id' => 1,
            'image' => '/images/matrimonio.jpg',
            'excerpt' => 'Los dos llegarán a ser un solo cuerpo. Así que ya no son dos, sino uno solo. Por tanto, lo que Dios ha unido, que no lo separe el hombre.',
            'body' => 'Asperiores dicta necessitatibus ea. Veritatis beatae similique accusantium ad omnis. Nihil laudantium quo dolor expedita. Quia qui voluptas ipsa omnis magni et aut voluptatem. Et molestiae explicabo delectus voluptas voluptates.',
            'active' => true,
        ]);

        Post::create([
            'title' => 'Confirmacion',
            'slug' => 'confirmacion',
            'user_id' => 1,
            'image' => '/images/confirmacion.jpg',
            'excerpt' => 'Acto Religioso que refuerza la gracia bautismal. Con este sacramento se acepta voluntariamente la fe de Cristo.',
            'body' => 'Asperiores dicta necessitatibus ea. Veritatis beatae similique accusantium ad omnis. Nihil laudantium quo dolor expedita. Quia qui voluptas ipsa omnis magni et aut voluptatem. Et molestiae explicabo delectus voluptas voluptates.',
            'active' => true,
        ]);

        Post::create([
            'title' => 'Bautizo',
            'slug' => 'bautizo',
            'user_id' => 1,
            'image' => '/images/bautizo.jpg',
            'excerpt' => 'Comienza a vivir bajo los ojos de Dios, sus manos te dan la bienvenida a este nuevo mundo.',
            'body' => 'Asperiores dicta necessitatibus ea. Veritatis beatae similique accusantium ad omnis. Nihil laudantium quo dolor expedita. Quia qui voluptas ipsa omnis magni et aut voluptatem. Et molestiae explicabo delectus voluptas voluptates.',
            'active' => true,
        ]);
        Post::create([
            'title' => 'Eucaristía',
            'slug' => 'eucaristia',
            'user_id' => 1,
            'image' => '/images/eucaristia.jpg',
            'excerpt' => 'La Eucaristía es misterio de fe, prenda de esperanza y fuente de caridad con Dios y entre los hombres.',
            'body' => 'Asperiores dicta necessitatibus ea. Veritatis beatae similique accusantium ad omnis. Nihil laudantium quo dolor expedita. Quia qui voluptas ipsa omnis magni et aut voluptatem. Et molestiae explicabo delectus voluptas voluptates.',
            'active' => true,
        ]);

        $nbrPosts = 10;

        // Post
        $posts = Post::all();

        // Set categories
        foreach ($posts as $post) {
            if ($post->id === 1) {
                DB::table ('category_post')->insert ([
                    'category_id' => 1,
                    'post_id' => 1,
                ]);
            }

            if ($post->id === 2) {
                DB::table ('category_post')->insert ([
                    'category_id' => 2,
                    'post_id' => 2,
                ]);
            }

            if ($post->id === 3) {
                DB::table ('category_post')->insert ([
                    'category_id' => 3,
                    'post_id' => 3,
                ]);
            }

            if ($post->id === 4) {
                DB::table ('category_post')->insert ([
                    'category_id' => 4,
                    'post_id' => 4,
                ]);
            }
            
        }
    }
}
