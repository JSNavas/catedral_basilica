<?php

use Illuminate\Database\Seeder;

class ConfirmantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

//  NOM_CONF	PAPA_CONF	MAMA_CONF	DIA_CONF	MES_CONF	ANO_CONF	PADRINO_1	PADRINO_2	LIBRO_CONF	PAG_CONF
//      0           1           2           3           4           5           6           7           8           9
    
    

    public function run()
    {
        function validateMounth($mounth){
            if($mounth == 'Enero'){
                return '1';
            }
            if($mounth == 'Febrero'){
                return '2';
            }
            if($mounth == 'Marzo'){
                return '3';
            }
            if($mounth == 'Abril'){
                return '4';
            }
            if($mounth == 'Mayo'){
                return '5';
            }
            if($mounth == 'Junio'){
                return '6';
            }
            if($mounth == 'Julio'){
                return '7';
            }
            if($mounth == 'Agosto'){
                return '8';
            }
            if($mounth == 'Septiembre'){
                return '9';
            }
            if($mounth == 'Octubre'){
                return '10';
            }
            if($mounth == 'Noviembre'){
                return '11';
            }
            if($mounth == 'Diciembre'){
                return '12';
            }
            else{
                return '01';
            }
        }

        function validateDay($day){
            if($day == '' || $day == 0){
                return '01';
            }
            if($day > 31){
                return '01';

            }
            if(!preg_match("/^[0-9]+$/", $day)){
                return '01';
            }
            else{
                return $day;
            }
        }
        function validateYear($year){
            if(!preg_match("/^[0-9]+$/", $year)){
                return '0000';
            }
            else{
                return $year;
            }
        }
        
        $fila = 1;
        if (($gestor = fopen("/home/navas/PROGRAMACION/LARAVEL/catedral_basilica/database/seeds/Confirma_base.csv", "r")) !== FALSE) {
            while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {
                $numero = count($datos);
                $fila++;
                for ($c=0; $c < $numero; $c++) {
                    // echo $datos[0];
                    
                }
                $fullName = explode(" ", $datos[0]);
                $name = join(' ', array_slice($fullName, -2));

                if(count($fullName) > 1){
                    $lastName = $fullName[0].' '.$fullName[1];
                    
                }else{
                    $lastName = $fullName[0];
                    
                }

                print_r($name);

                $mes = validateMounth($datos[4]);
                $dia = validateDay($datos[3]);
                $year = validateYear($datos[5]);

                DB::table('confirms')->insert([
                    [
                        'libro' => $datos[8],
                        'folio' => ($datos[9]) ? $datos[9] : 1,
                        'numero' => 1, 
                        'nombres' => $name,
                        'apellidos' => $lastName,
                        'parroquia' => 'Santa Ana Catedral Basílica Menor',
                        'sacerdote_celebrante_id' => 1,
                        'sacerdote_expide_id' => 1,
                        'sacerdote_firma_id' => 1,
                        'nombre_madre' => $datos[2],
                        'nombre_padre' => $datos[1],
                        'fecha_confirmacion' => $year.'-'.$mes.'-'.$dia,
                        'fecha_nacimiento' => '0000-01-01',
                        'fecha_bautismo' => '0000-01-01',
                        'fecha_expedicion' => '0000-01-01',
                        'lugar_nacimiento' => '_____',
                        'padrino_1' => $datos[6],
                        'padrino_2' => $datos[7],
                        'notas' => '',
                    ],
                ]);



            }
            fclose($gestor);

            }
        }
}
