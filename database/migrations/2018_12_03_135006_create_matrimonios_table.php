<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatrimoniosTable extends Migration {

	public function up()
	{
		Schema::create('matrimonios', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('libro');
            $table->integer('folio');
            $table->integer('numero');
			$table->string('parroquia');
            $table->string('nombres_esposo');
            $table->string('apellidos_esposo');
			$table->string('nombre_madre_esposo');
            $table->string('nombre_padre_esposo');
			$table->string('parroquia_bautismo_esposo');
			$table->string('ciudad_bautismo_esposo');
			$table->date('fecha_bautismo_esposo');
            $table->string('nombres_esposa');
            $table->string('apellidos_esposa');
			$table->string('nombre_madre_esposa');
            $table->string('nombre_padre_esposa');
			$table->string('parroquia_bautismo_esposa');
			$table->string('ciudad_bautismo_esposa');
			$table->date('fecha_bautismo_esposa');
			$table->date('fecha_matrimonio');
			$table->date('fecha_expedicion');
			$table->text('testigos');
			$table->integer('sacerdote_celebrante_id')->unsigned();
            $table->integer('sacerdote_expide_id')->unsigned();
			$table->integer('sacerdote_firma_id')->unsigned();
            $table->text('notas')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('matrimonios');
	}
}
