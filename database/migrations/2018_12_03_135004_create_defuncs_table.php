<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDefuncsTable extends Migration {

	public function up()
	{
		Schema::create('defuncs', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('libro');
            $table->integer('folio');
            $table->integer('numero');
			$table->string('parroquia');
            $table->string('nombres');
            $table->string('apellidos');
			$table->integer('edad');
            $table->string('nombre_madre');
            $table->string('nombre_padre');
            $table->string('nombre_conyuge');
            $table->string('causa_muerte');
			$table->date('fecha_funeral');
			$table->date('fecha_defuncion');
			$table->date('fecha_expedicion');
			$table->integer('sacerdote_celebrante_id')->unsigned();
            $table->integer('sacerdote_expide_id')->unsigned();
			$table->integer('sacerdote_firma_id')->unsigned();
            $table->text('notas')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('defuncs');
	}
}
