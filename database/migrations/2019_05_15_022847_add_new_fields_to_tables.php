<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewFieldsToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bautizos', function(Blueprint $table) {
			$table->string('hijo')->after('sacerdote_firma_id');
        });
        
        Schema::table('confirms', function(Blueprint $table) {
			$table->string('hijo')->after('sacerdote_firma_id');
        });
        
        Schema::table('eucaristias', function(Blueprint $table) {
			$table->string('hijo')->after('sacerdote_firma_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bautizos', function (Blueprint $table) {
            $table->dropColumn('hijo');
        });
        
        Schema::table('confirms', function (Blueprint $table) {
            $table->dropColumn('hijo');
        });

        Schema::table('eucaristias', function (Blueprint $table) {
            $table->dropColumn('hijo');
        });
    }
}
