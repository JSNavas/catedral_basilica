<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {


	public function up()
	{
		Schema::table('posts', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
					->onDelete('restrict')
					->onUpdate('restrict');
		});
		Schema::table('bautizos', function(Blueprint $table) {
			$table->foreign('sacerdote_celebrante_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_expide_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_firma_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
		});
		Schema::table('defuncs', function(Blueprint $table) {
			$table->foreign('sacerdote_celebrante_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_expide_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_firma_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
		});
		Schema::table('eucaristias', function(Blueprint $table) {
			$table->foreign('sacerdote_celebrante_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_expide_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_firma_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
		});
		Schema::table('confirms', function(Blueprint $table) {
			$table->foreign('sacerdote_celebrante_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_expide_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_firma_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
		});
		Schema::table('matrimonios', function(Blueprint $table) {
			$table->foreign('sacerdote_celebrante_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_expide_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
			$table->foreign('sacerdote_firma_id')->references('id')->on('sacerdotes')
					->onDelete('cascade')
					->onUpdate('cascade');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
					->onDelete('restrict')
					->onUpdate('restrict');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('posts')
					->onDelete('cascade')
					->onUpdate('cascade');
		});
		Schema::table('category_post', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('categories')
					->onDelete('cascade')
					->onUpdate('cascade');
		});
		Schema::table('category_post', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('posts')
					->onDelete('cascade')
					->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('posts', function(Blueprint $table) {
			$table->dropForeign('posts_user_id_foreign');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->dropForeign('comments_user_id_foreign');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->dropForeign('comments_post_id_foreign');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->dropForeign('comments_parent_id_foreign');
		});
		Schema::table('category_post', function(Blueprint $table) {
			$table->dropForeign('category_post_category_id_foreign');
		});
		Schema::table('category_post', function(Blueprint $table) {
			$table->dropForeign('category_post_post_id_foreign');
		});
	}
}
