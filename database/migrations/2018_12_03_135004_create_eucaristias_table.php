<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEucaristiasTable extends Migration {

	public function up()
	{
		Schema::create('eucaristias', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('libro');
            $table->integer('folio');
            $table->integer('numero');
            $table->string('nombres');
            $table->string('apellidos');
			$table->string('parroquia');
			$table->integer('sacerdote_celebrante_id')->unsigned();
            $table->integer('sacerdote_expide_id')->unsigned();
			$table->integer('sacerdote_firma_id')->unsigned();
			$table->string('nombre_madre');
            $table->string('nombre_padre');
			$table->date('fecha_nacimiento');
            $table->date('fecha_bautismo');
			$table->date('fecha_expedicion');
			$table->string('lugar_nacimiento');
			$table->string('padrino_1');
			$table->string('padrino_2');
            $table->text('notas')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('eucaristias');
	}
}
