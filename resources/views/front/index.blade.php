@extends('front.layout')

@section('main')

  <section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(/images/3531729169_ef59cff41f_a.jpg);">
    <div class="container">
      <div class="row align-items-center site-hero-inner justify-content-center">
        <div class="col-md-12 text-center">
  
          <div class="mb-5 element-animate">
            <h1>La Catedral Basílica</h1>
            <p>La Catedral de Coro es uno de los Monumentos más importantes y de gran significado para la Ciudad de Coro.</p>
            <div class="text-center">
              <a href="" class="btn btn-primary" data-toggle="modal" data-target="#modalLogin">ENTRAR</a>
            </div>
          </div>
  
        </div>
      </div>
    </div>
  </section>

    <section class="site-section">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-4">
            <div class="heading-wrap text-center element-animate">
              <h2 class="heading">Basílica Santa Ana de Coro</h2>
              <p class="mb-5">La Catedral fue entre todas las edificaciones construídas antes de 1713 la más importante ya
                que sentó precedente en
                las características arquitectónicas para la construcción de las Iglesias del resto del País y del
                Continente.</p>
            </div>
          </div>
          <div class="col-md-1"></div>
          <div class="col-md-7">
            <img src="images/Copia de Artist Interview_ Lizzie Strauss.png" style="width: 100%; height:100%;" alt="Image placeholder"
              class="img-md-fluid">
          </div>
        </div>
      </div>
    </section>

    <section class="site-section bg-light">
        <div>
          @isset($info)
              @component('front.components.alert')
                  @slot('type')
                      info
                  @endslot
                  {!! $info !!}
              @endcomponent
          @endisset
          @if ($errors->has('search'))
              @component('front.components.alert')
                  @slot('type')
                      error
                  @endslot
                  {{ $errors->first('search') }}
              @endcomponent
          @endif  


          <div class="container">
            <div class="row mb-5">
                <div class="col-md-12 heading-wrap text-center">
                <!-- <h4 class="sub-heading">Our Kind Staff</h4> -->
                <h2 class="heading">Realizamos</h2>
                </div>
            </div>

            <div class="row element-animate">          
                @foreach($posts as $post)

                    @include('front.brick-standard', compact('$post'))

                @endforeach
            </div>
          </div>
        </div> <!-- end row -->

        <div class="row">

          {{ $posts->links('front.pagination') }}

        </div>
    </section> <!-- end bricks -->

    <section class="section-cover" data-stellar-background-ratio="0.5" style="background-image: url(images/dsc_0179-2.jpg);">
      <div class="container">
        <div class="row justify-content-center align-items-center intro">
          <div class="col-md-9 text-center element-animate">
            <h2><strong>CATEDRAL DE CORO</strong></h2>
            <p class="lead mb-5">Fue la primera diócesis del continente por bula papal de 1531, territorio donde ejerce
              funciones un obispo,
              por lo cual su templo tiene categoría de catedral. Es un templo de suma sencillez construido en lo que se
              conoce en
              Venezuela como arquitectura colonial.</p>
    
          </div>
        </div>
      </div>
    </section>

@endsection
