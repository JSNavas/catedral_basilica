<div class="alert alert-{{ $type }} alert-dismissible fade show mb-5" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <p>{{ $slot }}</p>
</div>
