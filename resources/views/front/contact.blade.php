@extends('front.layout')

@section('main')

   <section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(/images/3531729169_ef59cff41f_a.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-12 text-center">

            <div class="mb-5 element-animate">
              <h1>Contáctanos</h1>
              <p></p>
            </div>

          </div>
        </div>
      </div>
		</section>

    <section class="site-section">
      <div class="container">
				@if (session('ok'))
						@component('front.components.alert')
								@slot('type')
										success
								@endslot
								{!! session('ok') !!}
						@endcomponent
				@endif

        <div class="row">
					<!--Grid column-->
					<div class="col-md-5 mb-md-0 mb-5">
						<h2 class="mb-5">Contáctanos</h2>

            <form action="{{ route('contacts.store') }}" method="POST">
								{{ csrf_field() }}
								
                <div class="row">
									
									<!--Grid column-->
									<div class="col-md-12">
										
										<div class="md-form mb-0">
											<input type="text" id="name-contact" name="name" class="form-control {{ $errors->has('name') ? 'invalid' : '' }}" value="{{ old('name') }}" required>
											<label for="name-contact" class="contact">Nombre</label>
											@if ($errors->has('name'))
													@component('front.components.error')
															{{ $errors->first('name') }}
													@endcomponent
											@endif 
											</div>
                    </div>
                    <!--Grid column-->            

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
											<div class="md-form mb-0">
												<input type="email" id="email-contact" name="email" class="form-control {{ $errors->has('email') ? 'invalid' : '' }}" value="{{ old('email') }}" required>
												<label for="email-contact" class="contact">Correo electronico</label>
												@if ($errors->has('email'))
														@component('front.components.error')
															Debe ser una dirección de correo electrónico válida.
														@endcomponent		
												@endif 
											</div>
											
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                            <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                            <label for="message" class="contact">Mensaje</label>
                        </div>

                    </div>
                </div>
								<!--Grid row-->
								
								<div class="text-center text-md-left">
										<button type="submit" class="btn btn-primary" >Enviar</button>
								</div>
								<div class="status"></div>

            </form>

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-3 text-center my-5">
            <ul class="list-unstyled my-5">
                <li><i class="fa fa-map-marker fa-2x"></i>
                    <p>Calle Federación, Coro, Falcón</p>
                </li>

                <li><i class="fa fa-phone mt-4 fa-2x"></i>
                    <p>0268-2515874</p>
                </li>

                <li><i class="fa fa-envelope mt-4 fa-2x"></i>
                    <p> catedraldecoro@gmail.com</p>
                </li>
            </ul>
        </div>
        <!--Grid column-->

              <div class="col-md-4">
								<h2 class="mb-5">Dirección</h2>
								
                <p class="mb-5"><img src="/images/googlemap-catedral.png" alt="" class="img-fluid"></p>
                
              </div>
        </div>
      </div>
    </section>
    <!-- END section -->
      
@endsection