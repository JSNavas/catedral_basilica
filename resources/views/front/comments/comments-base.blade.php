<li class="comment">

		<div class="vcard">
			<img src="{{ Gravatar::get($comment->user->email) }}" alt="">
		</div>

    <div class="comment-body">
			<div>
					<h3>{{ $comment->user->name }}</h3>

					@if(Auth::check() && Auth::user()->name == $comment->user->name)
							<a href="#" class="deletecomment"><span class="fa fa-fw fa-trash fa-lg  pull-right" title="@lang('Eliminar Comentario')"></span></a>
							<form action="{{ route('front.comments.destroy', [$comment->id]) }}" method="POST" class="hide">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
							</form>
							<a id="comment-edit{!! $comment->id !!}" href="#" class="editcomment"><span class="fa fa-fw fa-pencil fa-lg pull-right" title="@lang('Editar Comentario')"></span></a>
					@endif

					<div class="comment-meta">
							<div class="meta">{{ ucfirst (utf8_encode ($comment->created_at->formatLocalized('%A %d %B %Y'))) }}</div>
							
							<div class="comment-text">
									<p id="comment-body{{ $comment->id }}">{{ $comment->body }}</p>

									@if(Auth::check() && Auth::user()->name == $comment->user->name)
											<form id="comment-form{{ $comment->id }}" method="post" action="{{ route('comments.update', [$comment->id]) }}" class="comment-form hide">
													{{ csrf_field() }}
													</br>
													<div class="form-group shadow-textarea">
														<strong class="red"></strong>
														<textarea title="message" id="message{{ $comment->id }}" name="message{{ $comment->id }}" class="form-control z-depth-1" rows="3" required></textarea>
													</div>

													<button id="comment-escape{{ $comment->id }}" class="btn btn-close btnescape">@lang('Cerrar')</button>
													<button type="submit" class="btn btn-primary submit button-primary">@lang('Guardar')</button>
											</form>
									@endif

							</div>
							@if(Auth::check() && $level < config('app.commentsNestedLevel'))
									<p><a href="#" id="reply-create{!! $comment->id !!}" class="btn reply btn-primary">@lang('Responder')</a></p>
									<form id="reply-form{{ $comment->id }}" method="post" action="{{ route('posts.comments.comments.store', [$post->id, $comment->id]) }}" class="reply-form hide">
											{{ csrf_field() }}

											<div class="form-group shadow-textarea">
												<strong class="red"></strong>
												<textarea id="message{{ $comment->id }}" name="message{{ $comment->id }}" class="form-control z-depth-1" rows="3" placeholder="@lang('Tu respuesta...')" required></textarea>
											</div>

											<button id="reply-escape{{ $comment->id }}" class="btn btn-close btnescapereply">@lang('Cerrar')</button>
											<button type="submit" class="btn btn-primary submit button-primary">@lang('Enviar')</button>
									</form>
							@endif
					</div>
			</div>
		</div>
		
		@unless ($comment->isLeaf())
				@php
						$level++;
				@endphp
				<ul class="children">
						@include('front/comments/comments', ['comments' => $comment->getImmediateDescendants()])
				</ul>
		@endunless
	</li>
