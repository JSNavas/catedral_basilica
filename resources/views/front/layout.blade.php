<!DOCTYPE html>
<html lang="en" class="no-js" lang="{{ config('app.locale') }}">
 
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>CATEDRAL BASÍLICA</title>

  <!-- Font Awesome -->
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900|Rubik:300,400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="css/magnific-popup.css">

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
  @yield('css')
  
  
  <!-- Material Design Bootstrap -->
  <link href="{{ asset('css/mdb.css') }}" rel="stylesheet">

  <!-- Theme Style -->
  <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>

  <header role="banner">
  
    <nav class="navbar navbar-expand-md navbar-dark bg-light">
      <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}">Santa Ana de Coro</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05"
          aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse navbar-light" id="navbarsExample05">
              <ul class="navbar-nav ml-auto pl-lg-5 pl-0">
                <li {{ currentRoute('home') }} class="nav-item">
                  <a class="nav-link active" href="{{ route('home') }}">@lang('Inicio')</a>
								</li>
								
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('Categorias')</a>
                  <div class="dropdown-menu" aria-labelledby="dropdown04">
										@foreach ($categories as $category)
											<a class="dropdown-item" href="{{ route('category', [$category->slug ]) }}">{{ $category->title }}</a>
										@endforeach
                  </div>
                </li>
								@guest
									<li class="nav-item" {{ currentRoute('contacts.create') }}>
										<a class="nav-link" href="{{ route('contacts.create') }}">@lang('Contáctanos')</a>
									</li>
								@endguest

								@request('register')
									<li class="nav-item">
										<a class="nav-link" href="{{ request()->url() }}">@lang('Registro')</a>
									</li>
								@endrequest

								@request('password/email')
									<li class="nav-item">
										<a class="nav-link" href="{{ request()->url() }}">@lang('Contraseña olvidada')</a>
									</li>
								@else
									@guest
                  @request('password/reset')
                  <li class="nav-item">
                    <a class="nav-link" href="{{ request()->url() }}">@lang('Restablecer Contraseña')</a>
                  </li>
                  @endrequest
                  @request('password/reset/*')
                  <li class="nav-item">
                    <a class="nav-link" href="{{ request()->url() }}">@lang('Restablecer Contraseña')</a>
                  </li>
                  @endrequest
                  <li class="nav-item cta" {{ currentRoute('login') }}>
                    <a class="nav-link" data-toggle="modal" data-target="#modalLogin"><span>@lang('Iniciar')</span></a>
                  </li>
									@else
										@admin
											<li class="nav-item">
												<a class="nav-link" href="{{ url('admin') }}">@lang('Administración')</a>
											</li>
										@endadmin
										@redac
											<li class="nav-item">
												<a class="nav-link" href="{{ url('admin/posts') }}">@lang('Administración')</a>
											</li>
										@endredac
										<li class="nav-item cta">
                      <a class="nav-link" id="logout" href="{{ route('logout') }}"><span>@lang('Cerrar Sesión')</span></a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hide">
                        {{ csrf_field() }}
                      </form>
                    </li>
									@endguest
								@endrequest
              </ul>
              
            </div>
      </div>
    </nav>
	</header>
	

  <!-- Modal Login-->
  <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="modalLabelLogin" aria-hidden="true">
    <div class="modal-dialog form-dark" role="document">
      <!--Content-->
      <div class="modal-content card card-image" style="background-image: url('/images/8161992860_8ec1d49390_b.jpg');">
        <div class="text-white rgba-stylish-strong py-5 px-5 z-depth-4">
          <!--Header-->

          <div class="modal-header text-center pb-4">
            <h3 class="modal-title w-100 white-text font-weight-bold" id="modalLabelLogin"><strong>INICIAR</strong> <a class="brown-text font-weight-bold"><strong>
                  SESION</strong></a></h3>
            <button type="button" class="close white-text" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <!--Body-->

          @if (session('confirmation-success'))
              @component('front.components.alert')
                  @slot('type')
                      success
                  @endslot
                  {!! session('confirmation-success') !!}
              @endcomponent
          @endif
          @if (session('confirmation-danger'))
              @component('front.components.alert2')
                  @slot('type')
                      danger
                  @endslot
                  {!! session('confirmation-danger') !!}
              @endcomponent
          @endif

          <form role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            @if ($errors->has('log'))

              <div class="text-center">
                @component('front.components.error')
                <div class="alert alert-danger alertLogin alert-dismissible fade show text-center" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  {{ $errors->first('log') }}
                </div>
                @endcomponent
              </div>

    
            @endif
            <div class="modal-body">
              <!--Body-->
              <div class="md-form mb-5">
                <input id="log" name="log" type="email" class="form-control validate white-text {{ $errors->has('log') ? 'invalid' : '' }}" required>
                <label for="log">Correo electrónico</label>
              </div>
    
              <div class="md-form pb-3">
                <input id="password" name="password" type="password" class="form-control validate white-text {{ $errors->has('log') ? 'invalid' : '' }}" required>
                <label for="password">Contraseña</label>
                
                <div class="form-group mt-4">
                  <input class="form-check-input" type="checkbox" id="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                  <label for="checkbox" class="white-text form-check-label">Recordar Contraseña?</label>
                </div>
              </div>

    
              <div class="row d-flex align-items-center mb-4">
                <div class="text-center mb-3 col-md-12">
                  <button type="input" class="btn btn-primary btn-block z-depth-1">Iniciar</button>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <p class="font-small white-text d-flex justify-content-end">Tienes cuenta? <a id="link_register" href="{{ route('register') }}" class="brown-text ml-1 font-weight-bold">Regístrate!</a></p>
                </div>
                <div class="col-md-6">
                  <p class="font-small white-text d-flex justify-content-end">Olvidó su<a href="{{ route('password.request') }}" class="brown-text ml-1 font-weight-bold">Contraseña? </a></p>
                </div>


              </div>
          </form>
          </div>
        </div>
      </div>
      <!--/.Content-->
    </div>
  </div>
  <!-- Modal -->

  

  @yield('main')
  
  
  
  <footer class="site-footer">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-4">
          <h3>Numero de Contacto</h3>
          Telefono Fijo:
          <p class="lead"><span class="fa fa-phone-square"></span> 0268-2515874</p>
  
        </div>
        <div class="col-md-4">
          <h3>Contacta con nosotros</h3>
          Correo electrónico:
          <p class="lead"><span class="fa fa-envelope"></span> catedraldecoro@gmail.com</p>
        </div>
        <div class="col-md-4">
          <h3>Encuéntranos</h3>
          Direccion:
          <p class="lead"><span class="fa fa-map-marker"></span> Calle Federación, Coro, Falcón</p>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-md-7 text-center">
          &copy;
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          Copyright
          <script>document.write(new Date().getFullYear());</script> A.C Bmkeros R.L. All rights reserved
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </div>
      </div>
    </div>
  </footer>

  <!-- loader -->
  <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
      <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
      <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214" /></svg>
  </div>
  
  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery-migrate-3.0.0.js') }}"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.stellar.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/magnific-popup-options.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>


  <script>
	   $(function() {
		   $('#logout').click(function(e) {
			   e.preventDefault();
			   $('#logout-form').submit();
		   })

       $('#link_register').click(function(e) {
			   $('#modalLogin').modal('toggle');
		   })
	   })
  </script>

  @yield('scripts')
</body>

</html>
