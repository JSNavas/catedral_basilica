@extends('front.layout')

@section('css')
    @if (Auth::check())
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.css">
    @endif
@endsection

@section('main')

    <section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(/images/3531729169_ef59cff41f_a.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-12 text-center">

            <div class="mb-5 element-animate">
              <h1>{{ $post->title }}</h1>
              <p>{{$post->excerpt}}</p>
            </div>

          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <section class="site-section py-lg">
      <div class="container">
        
        <div class="row blog-entries">
          <div class="col-md-12 col-lg-8 main-content">
            <h1 class="mb-4">{{ $post->title }}</h1>
            <div class="post-meta">
							<span class="mr-2">{{ ucfirst (utf8_encode ($post->created_at->formatLocalized('%A %d %B %Y'))) }}</span>

						</div>
            <div class="post-content-body">
             
							<p>{!! $post->body !!}</p>
							<div class="row mb-5">
								<div class="col-md-12 mb-4 element-animate">
									<img src="{{ $post->image }}" alt="" class="container">
								</div>
							</div>      
            </div>

            
            <div class="pt-1">
              <p>
								Categories:
								@foreach ($post->categories as $category)
									<a href="{{ route('category', [$category->slug]) }}">{{ $category->title }}, </a>
								@endforeach
							</p>
						</div>
						
			
						
            <div class="pt-5">
							<h3 class="mb-5">{{ $post->valid_comments_count }} {{ trans_choice(__('comentario|comentarios'), $post->valid_comments_count) }}</h3>
              <ul class="comment-list">

								<li class="comment">
									<!-- commentlist -->
									@if ($post->valid_comments_count)
										@php
											$level = 0;
										@endphp

										<ol class="commentlist">
											@include('front/comments/comments', ['comments' => $post->parentComments])
										</ol>
											
										@if ($post->parent_comments_count > config('app.numberParentComments'))
											<p id="morebutton" class="text-center">
													<a id="nextcomments" href="{{ route('posts.comments', [$post->id, 1]) }}" class="btn btn-outline-primary">@lang('Más Comentarios')</a>
											</p>
													<p id="moreicon" class="text-center hide">
													<span class="fa fa-spinner fa-pulse fa-3x fa-fw"></span>
											</p>
										@endif
									@endif
								</li>

              </ul>
							<!-- END comment-list -->
							
              
              <div class="respond comment-form-wrap pt-5">
								
								@if (session('warning'))
										@component('front.components.alert')
												@slot('type')
													success
												@endslot
												{!! session('warning') !!}
										@endcomponent
								@endif
								
								<h3 class="mb-3">@lang('Deja un comentario')</h3>
								@if (Auth::check())
									<form method="post" action="{{ route('posts.comments.store', [$post->id]) }}" class="my-5 p-5 bg-light">
										{{ csrf_field() }}

										@if ($errors->has('message'))
											@component('front.components.error')
													{{ $errors->first('message') }}
											@endcomponent
										@endif

										<div class="md-form">
											<textarea type="text" name="message" id="message" class="md-textarea form-control" rows="3" value="{{ old('message') }}" required></textarea>
											<label for="message" class="send-comment">Comentario</label>
										</div>

										<button type="submit" class="btn btn-outline-primary submit">@lang('Enviar')</button>

									</form>
								@else
									<p class="comment">@lang('Debes estar registrado para añadir un comentario!')</p>
                @endif
              </div>
						</div>
          </div>


          <div class="col-md-12 col-lg-4 sidebar">
            <div class="sidebar-box search-form-wrap">
              <form role="search" method="get" class="search-form" action="{{ route('posts.search') }}" class="search-form">
                <div class="form-group">
                  <span class="icon fa fa-search"></span>
                  <input type="search" class="form-control search-field" placeholder="Buscar..." name="search" autocomplete="off" required>
                </div>
              </form>
            </div>
            <!-- END sidebar-box -->
            
            <div class="sidebar-box">
              <h3 class="heading">Otros</h3>
              <div class="post-entry-sidebar">
                <ul>
                  <li>
										@foreach($posts as $post)
											<a href="{{ url('posts/' . $post->slug) }}">
												<img src="{{ $post->image }}" alt="Image placeholder" class="mr-4">
												<div class="text">
													<h4>{{ $post->title }}</h4>
													<div class="post-meta">
														<span class="mr-2">{{ ucfirst (utf8_encode ($post->created_at->formatLocalized('%A %d %B %Y'))) }}</span>
													</div>
												</div>
											</a>
										</br>

                		@endforeach
                  </li>
                  
                </ul>
              </div>
            </div>
            <!-- END sidebar-box -->

            <div class="sidebar-box">
              <h3 class="heading">Categorías</h3>
              <ul class="categories">
								@foreach ($categories as $category)
									<li><a href="{{ route('category', [$category->slug ]) }}">{{ $category->title }} <span>({{ $post->categories->count() }})</span></a></li>
								@endforeach
              </ul>
            </div>
          </div>
          <!-- END sidebar -->

        </div>
			</div>
    </section>

   

@endsection

@section('scripts')
    @if (auth()->check())
        <script src="https://cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.js"></script>
        <script>

            var post = (function () {

                var onReady = function () {
                    $('body').on('click', 'a.deletecomment', deleteComment)
                        .on('click', 'a.editcomment', editComment)
                        .on('click', '.btnescape', escapeComment)
                        .on('submit', '.comment-form', submitComment)
                        .on('click', 'a.reply', replyCreation)
                        .on('click', '.btnescapereply', escapeReply)
                        .on('submit', '.reply-form', submitReply)
                }

                var toggleEditControls = function (id) {
                    $('#comment-edit' + id).toggle()
                    $('#comment-body' + id).toggle('slow')
                    $('#comment-form' + id).toggle('slow')
                }

                // Delete comment
                var deleteComment = function (event) {
                    event.preventDefault()
                    var that = $(this)
                    swal({
                        title: "{!! __('Seguro que quieres eliminar este comentario?') !!}",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{!! __('Si') !!}",
                        cancelButtonText: "{!! __('No') !!}"
                    }).then(function () {
                        that.next('form').submit()
                    })
                }

                // Set comment edition
                var editComment = function (event) {
                    event.preventDefault()
                    var i = $(this).attr('id').substring(12);
                    $('form.comment-form textarea#message' + i).val($('#comment-body' + i).text())
                    toggleEditControls(i)
                }

                // Escape comment edition
                var escapeComment = function (event) {
                    event.preventDefault()
                    var i = $(this).attr('id').substring(14)
                    toggleEditControls(i)
                    $('form.comment-form textarea#message' + i).prev().text('')
                }

                // Submit comment
                var submitComment = function (event) {
                    event.preventDefault();
                    $.ajax({
                        method: 'put',
                        url: $(this).attr('action'),
                        data: $(this).serialize()
                    })
                        .done(function (data) {
                            $('#comment-body' + data.id).text(data.message)
                            toggleEditControls(data.id)
                        })
                        .fail(function(data) {
                            var errors = data.responseJSON
                            $.each(errors, function(index, value) {
                                value = value[0].replace(index, '@lang('message')')
                                $('form.comment-form textarea[name="' + index + '"]').prev().text(value)
                            });
                        });
                }

                // Set reply creation
                var replyCreation = function (event) {
                    event.preventDefault()
                    var i = $(this).attr('id').substring(12)
                    $('form.reply-form textarea#message' + i).val('')
                    $('#reply-form' + i).toggle('slow')
                }

                // Escape reply creation
                var escapeReply = function (event) {
                    event.preventDefault()
                    var i = $(this).attr('id').substring(12)
                    $('#reply-form' + i).toggle('slow')
                }

                // Submit reply
                var submitReply = function (event) {
                    event.preventDefault()
                    $.ajax({
                        method: 'post',
                        url: $(this).attr('action'),
                        data: $(this).serialize()
                    })
                        .done(function (data) {
                            document.location.reload(true)
                        })
                        .fail(function(data) {
                            var errors = data.responseJSON
                            $.each(errors, function(index, value) {
                                value = value[0].replace(index, '@lang('message')')
                                $('form.reply-form textarea[name="' + index + '"]').prev().text(value)
                            });
                        });
                }

                return {
                    onReady: onReady
                }

            })()

            $(document).ready(post.onReady)

        </script>
    @endif

    <script>
        $(function() {
            // Get next comments
            $('#nextcomments').click (function(event) {
                event.preventDefault()
                $('#morebutton').hide()
                $('#moreicon').show()
                $.get($(this).attr('href'))
                .done(function(data) {
                    $('ol.commentlist').append(data.html)
                    if(data.href !== 'none') {
                        $('#nextcomments').attr('href', data.href)
                        $('#morebutton').show()
                    }
                    $('#moreicon').hide()
                })
            })
        })
    </script>
@endsection
