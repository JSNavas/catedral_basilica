@extends('front.layout')

@section('main')

   <section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(/images/3531729169_ef59cff41f_a.jpg);">
      <div class="container">
        <div class="row align-items-center site-hero-inner justify-content-center">
          <div class="col-md-12 text-center">

            <div class="mb-5 element-animate">
              <h1>Búsqueda</h1>
              <p></p>
            </div>

          </div>
        </div>
      </div>
    </section>

    <section class="site-section bg-light">
        <div class="container">
          @isset($info)
              @component('front.components.alert')
                  @slot('type')
                      info
                  @endslot
                  {!! $info !!}
              @endcomponent
          @endisset
          @if ($errors->has('search'))
              @component('front.components.alert')
                  @slot('type')
                      error
                  @endslot
                  {{ $errors->first('search') }}
              @endcomponent
          @endif  


          <div class="container">
            <div class="row mb-3">
                
            </div>

            <div class="row element-animate">          
                @foreach($posts as $post)

                    @include('front.brick-standard', compact('$post'))

                @endforeach
            </div>
          </div>
        </div> <!-- end row -->

        <div class="row">

          {{ $posts->links('front.pagination') }}

        </div>
    </section> <!-- end bricks -->



@endsection
