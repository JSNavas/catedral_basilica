<div class="col-md-3">
  <div class="post-entry">
		<div class="box">
				<a href="{{ url('posts/' . $post->slug) }}" class="thumb-link"><img src="{{ $post->image }}" alt="Image placeholder" class="img-fluid"></a>
				<div class="body-text">
					<!-- <div class="category">Staff</div> -->
					<h3 class="mb-3"><a href="{{ url('posts/' . $post->slug) }}">{{$post->title}}</a></h3>
					<strong>
						<p class="mb-4">{{$post->excerpt}}</p>
					</strong>
				</div>
		</div>
	</div>
</div>
