@extends('front.layout')

@section('main')
    <section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(/images/3531729169_ef59cff41f_a.jpg);">
		<div class="container">
			<div class="row align-items-center site-hero-inner justify-content-center">
				<div class="col-md-12 text-center">

					<div class="mb-5 element-animate">
						<h1>Iniciar sesion</h1>
						<p></p>
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="site-section">
		<div class="container">
			@if (session('confirmation-success'))
					@component('front.components.alert')
							@slot('type')
									success
							@endslot
							{!! session('confirmation-success') !!}
					@endcomponent
			@endif
			@if (session('confirmation-danger'))
					@component('front.components.alert')
							@slot('type')
									darger
							@endslot
							{!! session('confirmation-danger') !!}
					@endcomponent
			@endif

			@if ($errors->has('log'))
				@component('front.components.alert')
						@slot('type')
								danger
						@endslot
						{{ $errors->first('log') }}
				@endcomponent
			@endif
			
			<form role="form" method="POST" action="{{ route('login') }}">
			{{ csrf_field() }}
			
				<div class="row">
					<div class="col-md-1"></div>

					<div class="col-lg-10">
							<div class="row">

								<div class="col-md-3"></div>

								<div class="col-md-6">
									<div class="md-form mb-5">
										<input id="log" name="log" type="email" class="form-control {{ $errors->has('log') ? 'invalid' : '' }}" required>
                		<label for="log" class="label">Correo electrónico</label>								
									</div>
								</div>

								<div class="col-md-3"></div>
							</div>
			
							<div class="row">
								<div class="col-md-3"></div>

								<div class="col-md-6">
									<div class="md-form my-0 mb-5">
										<input id="password" name="password" type="password" class="form-control {{ $errors->has('log') ? 'invalid' : '' }}" required>
                		<label for="password" class="label">Contraseña</label>
									</div>
							<div class="text-center my-0">
									<button type="input" class="btn btn-primary btn-block z-depth-1">Iniciar</button>
							</div>
								</div>

								<div class="col-md-3"></div>
							</div>

					</div>

					<div class="col-md-1"></div>

				</div>
			</form>
		</div>
	</section>
@endsection
