@extends('front.layout')

@section('main')
	<section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(/images/3531729169_ef59cff41f_a.jpg);">
		<div class="container">
			<div class="row align-items-center site-hero-inner justify-content-center">
				<div class="col-md-12 text-center">

					<div class="mb-5 element-animate">
						<h1>Registro</h1>
						<p></p>
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="site-section">
		<div class="container">
			@if (session('confirmation-success'))
					@component('front.components.alert')
							@slot('type')
									success
							@endslot
							{!! session('confirmation-success') !!}
					@endcomponent
			@endif
			<form role="form" method="POST" action="{{ route('register') }}">
			{{ csrf_field() }}
				<div class="row">

					<div class="col-md-1"></div>

					<div class="col-lg-10">
							<h2 class="mb-5">Regístrate</h2>

							<div class="row">
								<div class="col-md-6">
									<div class="md-form mb-0">
										<input id="name-register" name="name" type="text" value="{{ old('name') }}" class="form-control {{ $errors->has('name') ? 'invalid' : '' }}" required>
										<label for="name-register" class="register">Nombre</label>		
										@if ($errors->has('name'))
												@component('front.components.error')
													{{ $errors->first('name') }}
												@endcomponent
										@endif 
									</div>
								</div>

								<div class="col-md-6">
									<div class="md-form mb-0">
										<input id="email" name="email" type="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? 'invalid' : '' }}" required>
										<label for="email" class="register">Correo electronico</label>
										@if ($errors->has('email'))
												@component('front.components.error')
													{{ $errors->first('email') }}
												@endcomponent
										@endif
									</div>
								</div>
							</div>
			
							<div class="row">
								<div class="col-md-6">
									<div class="md-form mb-0">
										<input id="password-register" name="password" type="password" class="form-control {{ $errors->has('password') ? 'invalid' : '' }}" required>
										<label for="password-register" class="register">Contraseña</label>
										@if ($errors->has('password'))
												@component('front.components.error')
													{{ $errors->first('password') }}
												@endcomponent
										@endif 
									</div>
								</div>

								<div class="col-md-6">
									<div class="md-form mb-0">
										<input id="password-confirm" name="password_confirmation" type="password" class="form-control {{ $errors->has('password') ? 'invalid' : '' }}" required>
										<label for="password-confirm" class="register">Confirmar contraseña</label>
									</div>
								</div>
							</div>

							<div class="text-center text-md-left my-3">
									<button type="submit" class="btn btn-primary" >Enviar</button>
							</div>
					</div>

					<div class="col-md-1"></div>

				</div>
			</form>
		</div>
	</section>

@endsection
