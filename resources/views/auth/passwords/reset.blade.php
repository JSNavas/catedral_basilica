@extends('front.layout')

@section('main')
    <section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(/images/3531729169_ef59cff41f_a.jpg);">
		<div class="container">
			<div class="row align-items-center site-hero-inner justify-content-center">
				<div class="col-md-12 text-center">

					<div class="mb-5 element-animate">
						<h1>Restablecer contraseña</h1>
						<p></p>
					</div>

				</div>
			</div>
		</div>
	</section>

    <section class="site-section">
		<div class="container">
			@if (session('status'))
					@component('front.components.alert')
							@slot('type')
									success
							@endslot
							<p>{{ session('status') }}</p>
					@endcomponent
			@endif
			<form role="form" method="POST" action="{{ route('password.request') }}">
			{{ csrf_field() }}
				<div class="row">

					<div class="col-md-1"></div>

					<div class="col-lg-10">
						<h2 class="mb-5">Restablecer contraseña</h2>

						<div class="row">
						
							<div class="col-md-3">
								<div class="md-form mb-0">
									<input type="hidden" name="token" value="{{ $token }}">
									<input id="email" name="email" type="email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? 'invalid' : '' }}" required>
									<label for="email" class="register">Correo electronico</label>
									@if ($errors->has('email'))
											@component('front.components.error')
													{{ $errors->first('email') }}
											@endcomponent
									@endif
								</div>
							</div>

							<div class="col-md-3">
								<div class="md-form mb-0">
									<input id="password-reset" name="password" type="password" class="form-control {{ $errors->has('password') ? 'invalid' : '' }}" required>
									<label for="password-reset" class="register">Contraseña</label>
									@if ($errors->has('password'))
											@component('front.components.error')
													{{ $errors->first('password') }}
											@endcomponent
									@endif 
								</div>
							</div>

							<div class="col-md-3">
								<div class="md-form mb-0">
									<input id="password-confirm" name="password_confirmation" type="password" class="form-control {{ $errors->has('password') ? 'invalid' : '' }}" required>
									<label for="password-confirm" class="register">Confirmar contraseña</label>
								</div>
							</div>
							<div class="col-md-1">
								<div class="text-center text-md-left my-3">
										<button type="submit" class="btn btn-primary" >Enviar</button>
								</div>
							</div>
						</div>
			
					</div>

					<div class="col-md-1"></div>

				</div>
			</form>
		</div>
	</section>
@endsection
