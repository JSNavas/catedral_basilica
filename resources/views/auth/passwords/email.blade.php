@extends('front.layout')

@section('main')
	<section class="site-hero site-hero-innerpage overlay" data-stellar-background-ratio="0.5" style="background-image: url(/images/3531729169_ef59cff41f_a.jpg);">
		<div class="container">
			<div class="row align-items-center site-hero-inner justify-content-center">
				<div class="col-md-12 text-center">

					<div class="mb-5 element-animate">
						<h1>Restablecer contraseña</h1>
						<p></p>
					</div>

				</div>
			</div>
		</div>
	</section>

	<section class="site-section">
		<p class="text-center w-responsive mx-auto mb-5">Has olvidado tu contraseña, no importa! Puedes crear uno nuevo. Pero para su propia seguridad queremos estar seguros de su identidad. Así que envíenos su correo electrónico rellenando este formulario. Recibirá un mensaje con instrucciones para crear su nueva contraseña.</p>
	
		<div class="container">
			@if (session('status'))
					@component('front.components.alert')
							@slot('type')
									success
							@endslot
							<p>{{ session('status') }}</p>
					@endcomponent
			@endif

			<form role="form" method="POST" action="{{ route('password.email') }}">
				{{ csrf_field() }}
				<div class="form-row align-items-center">
					<div class="col-md-3"></div>
					
					<div class="col-md-4">  
						<div class="md-form">
							<input id="email" name="email" type="email" class="form-control mb-2 {{ $errors->has('email') ? 'invalid' : '' }}" value="{{ old('email') }}" required>
							<label for="email" class="forgot-password">Correo electrónico</label>
							@if ($errors->has('email'))
									@component('front.components.error')
											{{ $errors->first('email') }}
									@endcomponent
							@endif   
						</div>
					</div>

					<div class="col-md-2">
						<button type="submit" class="btn btn-primary mb-0">Enviar</button>
					</div>

					<div class="col-md-3"></div>
				</div>
			</form>
		</div>
	</section>
@endsection
