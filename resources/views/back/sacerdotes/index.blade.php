@extends('back.layout')

@section('css')
    <link rel="stylesheet" href="//cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.css">
    <style>
        input, th span {
            cursor: pointer;
        }
        .pagination>li>a, .pagination>li>span {
            padding: 5px 10px;
            font-size: 12px;
            line-height: 1.5;
        }
        .pagination{
            margin: 0 !important;
        }
    </style>
@endsection

@section('button')
    <a class="btn btn-primary" href="{{ route('sacerdotes.create') }}">@lang('Nuevo Sacerdote')</a>
@endsection

@section('main')

    <div class="row">
        <div class="col-md-12">
            @if (session('sacerdote-ok'))
                @component('back.components.alert')
                    @slot('type')
                        success
                    @endslot
                    {!! session('sacerdote-ok') !!}
                @endcomponent
            @endif
            <div class="box">
                <div class="box-header with-border">
                    <div id="spinner" class="text-center"></div>
                </div>
                <div class="box-body table-responsive">
                    <table id="users" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>@lang('Nombre')</th>
                            <th>@lang('Título')</th>
                            @admin
                                <th></th>
                                <th></th>
                            @endadmin
                        </tr>
                        </thead>
                        <tbody id="pannel">
                            @include('back.sacerdotes.table', compact('sacerdotes'))
                        </tbody>
                    </table>
                </div>

                <div id="pagination" class="box-footer">
                    <div class="pagination-sm no-margin pull-right">
                        {{ $sacerdotes->links() }}
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

@endsection

@section('js')
    <script src="{{ asset('adminlte/js/back.js') }}"></script>
    <script>

        var sacerdote = (function () {

            var onReady = function () {
                $('#pannel').on('click', 'td a.btn-danger', function (event) {
                    var that = $(this)
                    event.preventDefault()
                    swal({
                        title: '@lang('Al eliminar este sacerdote se eliminará la información asociada a el. ¿Seguro de eliminar?')',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: '@lang('Si')',
                        cancelButtonText: '@lang('No')'
                    }).then(function () {
                        back.spin()
                        $.ajax({
                            url: that.attr('href'),
                            type: 'DELETE'
                        })
                            .done(function () {
                                that.parents('tr').remove()
                                back.unSpin()
                            })
                            .fail(function () {
                                back.fail('@lang('Parece que hay un problema con el servidor...')')
                            }
                        )
                    })
                })
            }

            return {
                onReady: onReady
            }

        })()

        $(document).ready(sacerdote.onReady)

    </script>
@endsection