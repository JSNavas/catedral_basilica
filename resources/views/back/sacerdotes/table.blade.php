@foreach($sacerdotes as $sacerdote)
    <tr>
        <td>{{ $sacerdote->nombre }}</td>
        <td>{{ $sacerdote->titulo }}</td>
        @admin
            <td><a class="btn btn-warning btn-xs btn-block" href="{{ route('sacerdotes.edit', [$sacerdote->id]) }}" role="button" title="@lang('Editar')"><span class="fa fa-edit"></span></a></td>
            <td><a class="btn btn-danger btn-xs btn-block" href="{{ route('sacerdotes.destroy', [$sacerdote->id]) }}" role="button" title="@lang('Eliminar')"><span class="fa fa-remove"></span></a></td>
        @endadmin
    </tr>
@endforeach

