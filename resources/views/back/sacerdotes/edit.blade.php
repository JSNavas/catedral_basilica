@extends('back.sacerdotes.template')

@section('form-open')
    <form method="post" action="{{ route('sacerdotes.update', [$sacerdote->id]) }}">
        {{ method_field('PUT') }}
@endsection
