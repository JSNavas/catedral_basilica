@extends('back.layout')

@section('js')
    <script src="{{ asset ('adminlte/js/jquery.inputmask.js') }}"></script>
    <script src="{{ asset ('adminlte/js/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset ('adminlte/js/jquery.inputmask.extensions.js') }}"></script>

    <script>
        $(function () {
            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy/mm/dd', { 'placeholder': 'yyyy/mm/dd' })

            //Money Euro
            $('[data-mask]').inputmask()
        })
    </script>
@endsection

@section('main')

    @yield('form-open')
        {{ csrf_field() }}

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Defunción</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row">

                    <div class="col-md-2">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Libro',
                                'name' => 'libro',
                                'value' => isset($defunc) ? $defunc->libro : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-2">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Folio',
                                'name' => 'folio',
                                'value' => isset($defunc) ? $defunc->folio : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-2">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Número',
                                'name' => 'numero',
                                'value' => isset($defunc) ? $defunc->numero : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-3">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Parroquia',
                                'name' => 'parroquia',
                                'value' => isset($defunc) ? $defunc->parroquia : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Sacerdote Celebrante</label>
                            <select class="form-control select2" style="width: 100%;" id="sacerdote_celebrante_id" name="sacerdote_celebrante_id" required>
                                <option value>Seleccione un sacerdote</option>
                                @foreach ($sacerdotes as $sacerdote)

                                    @if(isset($defunc) && $defunc->sacerdote_celebrante_id == $sacerdote->id)
                                        <option value="{{ $sacerdote->id }}" selected>{{ $sacerdote->nombre }}</option>
                                    @else
                                        <option value="{{ $sacerdote->id }}">{{ $sacerdote->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombres',
                                'name' => 'nombres',
                                'value' => isset($defunc) ? $defunc->nombres : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Apellidos',
                                'name' => 'apellidos',
                                'value' => isset($defunc) ? $defunc->apellidos : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Edad',
                                'name' => 'edad',
                                'value' => isset($defunc) ? $defunc->edad : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>


                </div>

                <div class="row">

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombre de la Madre',
                                'name' => 'nombre_madre',
                                'value' => isset($defunc) ? $defunc->nombre_madre : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombre del Padre',
                                'name' => 'nombre_padre',
                                'value' => isset($defunc) ? $defunc->nombre_padre : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombre del Conyuge',
                                'name' => 'nombre_conyuge',
                                'value' => isset($defunc) ? $defunc->nombre_conyuge : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>    
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha de Funeral</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>

                                @if(isset($defunc) && $defunc->fecha_funeral)
                                    <input type="text" value="{{$defunc->fecha_funeral}}" class="form-control" id="fecha_funeral" name="fecha_funeral" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_funeral" name="fecha_funeral" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha de Defunción</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>

                                @if(isset($defunc) && $defunc->fecha_defuncion)
                                    <input type="text" value="{{$defunc->fecha_defuncion}}" class="form-control" id="fecha_defuncion" name="fecha_defuncion" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_defuncion" name="fecha_defuncion" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                       <div class="form-group">
                            <label>Fecha de Expedición</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                                @if(isset($defunc) && $defunc->fecha_expedicion)
                                    <input type="text" value="{{$defunc->fecha_expedicion}}" class="form-control" id="fecha_expedicion" name="fecha_expedicion" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_expedicion" name="fecha_expedicion" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Causa de Muerte',
                                'name' => 'causa_muerte',
                                'value' => isset($defunc) ? $defunc->causa_muerte : '',
                                'input' => 'text',
                                'required' => false,
                            ],
                        ])
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Sacerdote que expide original</label>
                            <select class="form-control select2" style="width: 100%;" id="sacerdote_expide_id" name="sacerdote_expide_id" required>
                                <option value>Seleccione un sacerdote</option>
                                @foreach ($sacerdotes as $sacerdote)

                                    @if(isset($defunc) && $defunc->sacerdote_expide_id == $sacerdote->id)
                                        <option value="{{ $sacerdote->id }}" selected>{{ $sacerdote->nombre }}</option>
                                    @else
                                        <option value="{{ $sacerdote->id }}">{{ $sacerdote->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Sacerdote que firma copia u original</label>
                            <select class="form-control select2" style="width: 100%;" id="sacerdote_firma_id" name="sacerdote_firma_id" required>
                                <option value>Seleccione un sacerdote</option>
                                @foreach ($sacerdotes as $sacerdote)

                                    @if(isset($defunc) && $defunc->sacerdote_firma_id == $sacerdote->id)
                                        <option value="{{ $sacerdote->id }}" selected>{{ $sacerdote->nombre }}</option>
                                    @else
                                        <option value="{{ $sacerdote->id }}">{{ $sacerdote->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Notas',
                                'name' => 'notas',
                                'value' => isset($defunc) ? $defunc->notas : '',
                                'input' => 'textarea',
                                'rows' => '2',
                                'required' => false,
                            ],
                        ])
                    </div>
                </div>
            </div>      
        </div>

        <button type="submit" class="btn btn-primary">@lang('Guardar')</button>
    </form>

@endsection

@section('js')

    <script src="{{ asset('adminlte/plugins/voca/voca.min.js') }}"></script>
    <script>

        $('#slug').keyup(function () {
            $(this).val(v.slugify($(this).val()))
        })

        $('#title').keyup(function () {
            $('#slug').val(v.slugify($(this).val()))
        })

    </script>

@endsection