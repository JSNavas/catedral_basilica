@extends('back.defuncs.template')

@section('form-open')
    <form method="post" action="{{ route('defuncs.update', [$defunc->id]) }}">
        {{ method_field('PUT') }}
@endsection
