<html>

<head>
    <style>
        body {
            font-family: arial;
						font-size: 20px;
        }

        @page {
            margin: 160px 50px;
        }

        header {
            position: fixed;
            left: 20px;
            top: -160px;
            right: 0px;
            height: 100px;
            background-color: #fff;
            text-align: center;
        }

        header h1 {
            margin: 10px 0;
        }

        header h2 {
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -50px;
            right: 0px;
            height: 40px;
        }
        .firma {
					margin: 0px;
        }

        footer .page:after {
            content: counter(page);
        }

        footer table {
            width: 100%;
        }

        footer p {
            text-align: right;
        }

        footer .izquierda {
						position: absolute;
						top: 80px;
        }
        footer .derecha {
						position: absolute;
            top: 83px;
						right: 10px;
        }

        footer .center {
						margin-left: 65px;
            text-align: center;
        }

				.tab {
					text-indent: 2em;
					text-align: justify;
  				text-justify: inter-word;
				}
				
				.contenedor {
					line-height: 170% !important;
					text-align: center;
				}

				.align {
					line-height: 170% !important;
					text-align: center;
				}

				.subrayado {
					text-decoration: underline;
				}

				.footer {
					text-align: center;
					font-size: 14px;
					position: absolute;
					top: 80px;
				}
				.firma {
					text-align: center;
					position: absolute;
					bottom: 145px;
				}
				.centrar {
					text-align: center;
				}
    </style>


<body>
    <header>
        <br/><br/>
				<img src="{{ public_path() }}/images/logocatedral.png" height="100" width="100">
        <h1>Arquidiócesis de Coro - Venezuela</h1>
        <h2 style="font-weight: 300;">Acta de Defunción</h2>
    </header>


    <div id="content">
		<br/>
		<br/>
		<br/>
        <br/>
		<br/>
        <br/>
				
        <p class="tab contenedor">
			El suscrito Párroco de la Parroquia {{ $defunc->parroquia }} certifica que: En el libro de defunción correspondiente a la página {{ $defunc->libro }} Nº {{ $defunc->numero }}, se encuentra asentada un acta de defunción que textualmente dice así: 
			<br/>
        	<br/>
        </p>

        <p class="tab contenedor">
					A {{ Carbon\Carbon::parse($defunc->fecha_funeral)->format('d/m/Y') }}, el Cura Párroco de Santa Ana de Coro, hizo los oficios de Sepultura Eclesiástica al Cadáver de
					<strong style="text-transform: uppercase;">{{ $defunc->nombres.' '.$defunc->apellidos }}</strong> de {{ $defunc->edad }} años
					de edad, Hijo de {{ $defunc->nombre_madre.' y '.$defunc->nombre_padre }}. Casado
					con {{ $defunc->nombre_conyuge }}. Muerte: {{ $defunc->causa_muerte }}, 
					el {{ Carbon\Carbon::parse($defunc->fecha_defuncion)->format('d/m/Y') }}. Doy fé, {{ $defunc->sacerdoteExpide->titulo.', '.$defunc->sacerdoteExpide->nombre }}.
					Es fiel copia de original, expedida en Coro, Estado Falcón, el 
					{{ Carbon\Carbon::parse($defunc->fecha_expedicion)->format('d/m/Y') }}. Doy fé:
        </p>
					

    </div>

    <footer>
        <table>
					<tr>
							<td>
								<div class="footer">
									<hr/>
									Catedral Basílica Menor, Calle Palmasola entre calle Ciencias y Federacion,
									Frente a la plaza Bolivar. Coro. Edo. Falcón - Venezuela Teléfono de contacto +58 (0268)2515874.
									Email: Catedraldecoro@gmail.com
								</div>
							</td>
					</tr>
					<tr>

							<td>
								<div class="firma">
									<hr style="width: 250px; position: relative;top:14px;"/>
									<p class="centrar">
												{{ $defunc->sacerdoteFirma->nombre }}
												<br/>
												{{ $defunc->sacerdoteFirma->titulo }}
										</p>
								</div>
							</td>
					</tr>
        </table>
    </footer>
</body>

</html>