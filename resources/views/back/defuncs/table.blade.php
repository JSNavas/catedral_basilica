@foreach($defuncs as $defunc)
    <tr>
        <td>{{ $defunc->libro }}</td>
        <td>{{ $defunc->folio }}</td>
        <td>{{ $defunc->numero }}</td>
        <td>{{ $defunc->parroquia }}</td>
        <td>{{ $defunc->nombres }}</td>
        <td>{{ $defunc->apellidos }}</td>
        <td>{{ $defunc->edad }}</td>
        <td>{{ $defunc->sacerdoteFirma->nombre }}</td>
        <td>{{ $defunc->fecha_funeral }}</td>
        <td>{{ $defunc->fecha_defuncion }}</td>
        <td>{{ $defunc->fecha_expedicion }}</td>
        @admin
            <td><a class="btn btn-warning btn-xs btn-block" href="{{ route('defuncs.edit', [$defunc->id]) }}" role="button" title="@lang('Editar')"><span class="fa fa-edit"></span></a></td>
            <td><a class="btn btn-danger btn-xs btn-block" href="{{ route('defuncs.destroy', [$defunc->id]) }}" role="button" title="@lang('Eliminar')"><span class="fa fa-remove"></span></a></td>
        @endadmin
        <td><a class="btn btn-info btn-xs btn-block" href="{{ route('defuncs.export_pdf', [$defunc->id]) }}" role="button" title="@lang('PDF')"><span class="fa fa-download"></span></a></td>
    </tr>
@endforeach

