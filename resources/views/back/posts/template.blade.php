@extends('back.layout')

@section('css')
    <style>
        textarea { resize: vertical; }
    </style>
    <link href="{{ asset('adminlte/plugins/colorbox/colorbox.css') }}" rel="stylesheet">
@endsection

@section('main')

    @yield('form-open')
        {{ csrf_field() }}

        <div class="row">

            <div class="col-md-8">
                @if (session('post-ok'))
                    @component('back.components.alert')
                        @slot('type')
                            success
                        @endslot
                        {!! session('post-ok') !!}
                    @endcomponent
                @endif
                @include('back.partials.boxinput', [
                    'box' => [
                        'type' => 'box-primary',
                        'title' => __('Título'),
                    ],
                    'input' => [
                        'name' => 'title',
                        'value' => isset($post) ? $post->title : '',
                        'input' => 'text',
                        'required' => true,
                    ],
                ])
                @include('back.partials.boxinput', [
                    'box' => [
                        'type' => 'box-primary',
                        'title' => __('Extracto'),
                    ],
                    'input' => [
                        'name' => 'excerpt',
                        'value' => isset($post) ? $post->excerpt : '',
                        'input' => 'textarea',
                        'rows' => 3,
                        'required' => true,
                    ],
                ])
                @include('back.partials.boxinput', [
                    'box' => [
                        'type' => 'box-primary',
                        'title' => __('Contenido'),
                    ],
                    'input' => [
                        'name' => 'body',
                        'value' => isset($post) ? $post->body : '',
                        'input' => 'textarea',
                        'rows' => 10,
                        'required' => true,
                    ],
                ])
                <button type="submit" class="btn btn-primary">@lang('Publicar')</button>
            </div>

            <div class="col-md-4">

                @component('back.components.box')
                    @slot('type')
                        warning
                    @endslot
                    @slot('boxTitle')
                        @lang('Categorías')
                    @endslot
                    @include('back.partials.input', [
                        'input' => [
                            'name' => 'categories',
                            'values' => isset($post) ? $post->categories : collect(),
                            'input' => 'select',
                            'options' => $categories,
                        ],
                    ])
                @endcomponent

                @component('back.components.box')
                    @slot('type')
                        success
                    @endslot
                    @slot('boxTitle')
                        @lang('Detalles')
                    @endslot
                    @include('back.partials.input', [
                        'input' => [
                            'name' => 'slug',
                            'value' => isset($post) ? $post->slug : '',
                            'input' => 'text',
                            'title' => __('Índice'),
                            'required' => true,
                        ],
                    ])
                    @include('back.partials.input', [
                        'input' => [
                            'name' => 'active',
                            'value' => isset($post) ? $post->active : false,
                            'input' => 'checkbox',
                            'title' => __('Estado'),
                            'label' => __('Activo'),
                        ],
                    ])
                @endcomponent

                @component('back.components.box')
                    @slot('type')
                        primary
                    @endslot
                    @slot('boxTitle')
                        @lang('Imagen')
                    @endslot
                    <img id="img" src="@isset($post) {{ $post->image }} @endisset" alt="" class="img-responsive">
                    @slot('footer')
                        <div class="{{ $errors->has('image') ? 'has-error' : '' }}">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <a href="" class="popup_selector btn btn-primary" data-inputid="image">@lang('Seleccione una imagen')</a>
                                </div>
                                <!-- /btn-group -->
                                <input class="form-control" type="text" id="image" name="image" value="{{ old('image', isset($post) ? $post->image : '') }}">
                            </div>
                            {!! $errors->first('image', '<span class="help-block">:message</span>') !!}
                        </div>
                    @endslot
                @endcomponent 

        </div>
        </div>
        <!-- /.row -->
    </form>

@endsection

@section('js')

    <script src="{{ asset('adminlte/plugins/colorbox/jquery.colorbox-min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/voca/voca.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/ckeditor/ckeditor.js') }}"></script>
    <script>
    
        $('#slug').keyup(function () {
            $(this).val(v.slugify($(this).val()))
        })

        $('#title').keyup(function () {
            $('#slug').val(v.slugify($(this).val()))
        })

        CKEDITOR.replace('body', {customConfig: '/adminlte/js/ckeditor.js'})

        $('.popup_selector').click( function (event) {
            event.preventDefault()
            var updateID = $(this).attr('data-inputid')
            var elfinderUrl = '/elfinder/popup/'
            var triggerUrl = elfinderUrl + updateID
            $.colorbox({
                href: triggerUrl,
                fastIframe: true,
                iframe: true,
                width: '70%',
                height: '70%'
            })
        })

        function processSelectedFile(filePath, requestingField) {
            $('#' + requestingField).val('\\' + filePath)
            $('#img').attr('src', '\\' + filePath)
        }


    </script>

@endsection