@extends('back.layout')

@section('css')
    <style>
        .box-body hr+p {
            font-size: x-large;
        }
    </style>
@endsection


@section('main')

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <hr>
                    <p>ID</p>
                    {{ $post->id }}
                    <hr>
                    <p>@lang('Título')</p>
                    {{ $post->title }}
                    <hr>
                    <p>@lang('Autor')</p>
                    {{ $post->user->name }}
                    <hr>
                    <p>@lang('Extracto')</p>
                    {{ $post->excerpt }}
                    <hr>
                    <p>@lang('Contenido')</p>
                    {!! $post->body !!}
                    <hr>
                    <p>@lang('Imagen')</p>
                    <img src="{{ $post->image }}" alt="" width="200px">
                    <hr>
                    <p>@lang('Categorías')</p>
                    @foreach($post->categories as $category)
                        {{ $category->title }}<br>
                    @endforeach
                    <hr>
                    <p>@lang('Índice')</p>
                    {{ $post->slug }}
                    <hr>
                    <p>@lang('Estado')</p>
                    {{ $post->active ? __('Active') : __('No Active')}}
                    <hr>
                    <p>@lang('Fecha de Creación')</p>
                    {{ $post->created_at->formatLocalized('%c') }}
                    <hr>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

@endsection