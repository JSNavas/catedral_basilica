<html>

<head>
    <style>
        body {
            font-family: arial;
						font-size: 20px;
        }

        @page {
            margin: 160px 50px;
        }

        header {
            position: fixed;
            left: 20px;
            top: -160px;
            right: 0px;
            height: 100px;
            background-color: #fff;
            text-align: center;
        }

        header h1 {
            margin: 10px 0;
        }

        header h2 {
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -50px;
            right: 0px;
            height: 40px;
        }
        .firma {
					margin: 0px;
        }

        footer .page:after {
            content: counter(page);
        }

        footer table {
            width: 100%;
        }

        footer p {
            text-align: right;
        }

        footer .izquierda {
						position: absolute;
						top: 80px;
        }
        footer .derecha {
						position: absolute;
            top: 83px;
						right: 10px;
        }

        footer .center {
						margin-left: 65px;
            text-align: center;
        }

				.tab {
					text-indent: 2em;
					text-align: justify;
  				text-justify: inter-word;
				}
				
				.contenedor {
					line-height: 200% !important;
				}

				.align {
					line-height: 170% !important;
					text-align: center;
				}

				.subrayado {
					text-decoration: underline;
				}

				.footer {
					text-align: center;
					font-size: 14px;
					position: absolute;
					top: 80px;
				}
				.firma {
					text-align: center;
					position: absolute;
					bottom: 60px;
				}
				.centrar {
					text-align: center;
				}
    </style>

<body>
    <header>
        <br/><br/>
				<img src="{{ public_path() }}/images/logocatedral.png" height="100" width="100">
				<!-- <img src="/home/lacatedr/public_html/images/logocatedral.png" height="100" width="100"> -->
        <h1>Arquidiócesis de Coro - Venezuela</h1>
        <h2 style="font-weight: 300;">Certificado de Bautismo</h2>
    </header>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>

		<p class="contenedor">
			El párroco de la {{ $bautizo->parroquia }}, certifica que en el libro de Bautismo <strong>Nº {{ $bautizo->libro }}</strong>
			Página <strong>{{ $bautizo->folio }} Nº {{ $bautizo->numero }}</strong> de la Parroquia Santa Ana, se encuentra asentada la Partida de Bautismo de:
		</p>

    <div class="align">
			<strong class="subrayado" style="text-align: center;">{{ $bautizo->nombres.' '.$bautizo->apellidos }}</strong>
    </div>

    <div id="content">
        <p class="contenedor">
			Que nació el: <strong class="subrayado">{{ Carbon\Carbon::parse($bautizo->fecha_nacimiento)->format('d/m/Y') }}</strong><br/>					
			Bautizado el día: <strong class="subrayado"> {{ Carbon\Carbon::parse($bautizo->fecha_bautismo)->format('d/m/Y') }} </strong><br/>
			Se bautizó solemnemente por el: <strong class="subrayado"> {{ $bautizo->sacerdoteCelebrante->titulo.', '.$bautizo->sacerdoteCelebrante->nombre }} </strong><br/>
			Hijo (a) <strong class="subrayado">{{ $bautizo->hijo }}</strong> de: <strong class="subrayado">{{ $bautizo->nombre_madre }}</strong> y <strong class="subrayado">{{ $bautizo->nombre_padre }}</strong><br/>
			Fueron sus Padrinos: 
			
				@if($bautizo->padrino_1 AND $bautizo->padrino_2)
				<strong class="subrayado"> 
					{{ $bautizo->padrino_1 }}
				</strong> 
				y 
				<strong class="subrayado">{{ $bautizo->padrino_2 }} </strong>
			@else
				<strong class="subrayado"> 
					{{ $bautizo->padrino_1 }}
				</strong> 
			@endif 
			<br/>
			Doy fé <strong class="subrayado"> {{ $bautizo->sacerdoteExpide->titulo.', '.$bautizo->sacerdoteExpide->nombre }} </strong><br/>
			Observaciones: <strong class="subrayado">{{ $bautizo->notas }}</strong><br/>
			Es fiel copia de original, expedida en <strong>Coro, Estado Falcón, el {{ Carbon\Carbon::parse($bautizo->fecha_expedicion)->format('d/m/Y') }}</strong><br/>
			Doy fé:
        </p>
    </div>

    <footer>
        <table>
						<tr>
                <td>
									<div class="footer">
										<hr/>
										Catedral Basílica Menor, Calle Palmasola entre calle Ciencias y Federacion,
										Frente a la plaza Bolivar. Coro. Edo. Falcón - Venezuela Teléfono de contacto +58 (0268)2515874.
										Email: Catedraldecoro@gmail.com
									</div>
                </td>
            </tr>
						<tr>

                <td>
									<div class="firma">
										<hr style="width: 250px; position: relative;top:14px;"/>
										<p class="centrar">
													{{ $bautizo->sacerdoteFirma->nombre }}
													<br/>
													{{ $bautizo->sacerdoteFirma->titulo }}
											</p>
									</div>
                </td>
            </tr>
        </table>
    </footer>
</body>

</html>