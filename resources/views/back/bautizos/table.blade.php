@foreach($bautizos as $bautizo)
    <tr>
        <td>{{ $bautizo->libro }}</td>
        <td>{{ $bautizo->folio }}</td>
        <td>{{ $bautizo->numero }}</td>
        <td>{{ $bautizo->parroquia }}</td>
        <td>{{ $bautizo->nombres }}</td>
        <td>{{ $bautizo->apellidos }}</td>
        <td>{{ $bautizo->sacerdoteFirma->nombre }}</td>
        <td>{{ $bautizo->fecha_nacimiento }}</td>
        <td>{{ $bautizo->fecha_bautismo }}</td>
        <td>{{ $bautizo->fecha_expedicion }}</td>
        @admin
            <td><a class="btn btn-warning btn-xs btn-block" href="{{ route('bautizos.edit', [$bautizo->id]) }}" role="button" title="@lang('Editar')"><span class="fa fa-edit"></span></a></td>
            <td><a class="btn btn-danger btn-xs btn-block" href="{{ route('bautizos.destroy', [$bautizo->id]) }}" role="button" title="@lang('Eliminar')"><span class="fa fa-remove"></span></a></td>
        @endadmin
        <td><a class="btn btn-info btn-xs btn-block" href="{{ route('bautizos.export_pdf', [$bautizo->id]) }}" role="button" title="@lang('PDF')"><span class="fa fa-download"></span></a></td>
    </tr>
@endforeach

