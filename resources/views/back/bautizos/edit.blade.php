@extends('back.bautizos.template')

@section('form-open')
    <form method="post" action="{{ route('bautizos.update', [$bautizo->id]) }}">
        {{ method_field('PUT') }}
@endsection
