@extends('back.layout')

@section('css')
    <link rel="stylesheet" href="//cdn.jsdelivr.net/sweetalert2/6.3.8/sweetalert2.min.css">
    <style>
        input, th span {
            cursor: pointer;
        }
        .pagination>li>a, .pagination>li>span {
            padding: 5px 10px;
            font-size: 12px;
            line-height: 1.5;
        }
        .pagination{
            margin: 0 !important;
        }
    </style>
@endsection

@section('button')
    <a class="btn btn-primary" href="{{ route('confirms.create') }}">@lang('Nueva Confirmación')</a>
@endsection

@section('main')

    <form method="post" action="{{ route('confirms.index') }}" class="navbar-form pull-right">
        {{ csrf_field() }}

        <input type="radio" name="type" value="libro" checked> @lang('Libro')&nbsp;&nbsp;
        <input type="radio" name="type" value="folio" {{ $type == "folio" ? 'checked' : '' }}> @lang('Folio')&nbsp;&nbsp;
        <input type="radio" name="type" value="numero" {{ $type == "numero" ? 'checked' : '' }}> @lang('Numero')&nbsp;&nbsp;
        <input type="radio" name="type" value="nombres" {{ $type == "nombres" ? 'checked' : '' }}> @lang('Nombres')&nbsp;&nbsp;
        <input type="radio" name="type" value="apellidos" {{ $type == "apellidos" ? 'checked' : '' }}> @lang('Apellidos')&nbsp;&nbsp;
        <input type="radio" name="type" value="fecha_expedicion" {{ $type == "fecha_expedicion" ? 'checked' : '' }}> @lang('Fecha de Expedicion')&nbsp;&nbsp;
        
        <div class="input-group"> 
            <input type="text" class="form-control" name="search" placeholder="Busqueda"> 
                <span class="input-group-btn"> 
                <button type="submit" class="btn btn-default"> 
                    <span class="fa fa-search"></span> 
                </button> 
                <button type="submit" class="btn btn-default"> 
                    <span class="fa fa-refresh"></span> 
                </button> 
            </span> 
        </div> 
    </form>

    <div class="navbar-form pull-left">
        {{ isset($search) ? 'Búsqueda: '.$search : '' }}
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (session('confirmacion-ok'))
                @component('back.components.alert')
                    @slot('type')
                        success
                    @endslot
                    {!! session('confirmacion-ok') !!}
                @endcomponent
            @endif
            <div class="box">
                <div class="box-header with-border">
                    <div id="spinner" class="text-center"></div>
                </div>
                <div class="box-body table-responsive">
                    <table id="users" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>@lang('libro')</th>
                            <th>@lang('Folio')</th>
                            <th>@lang('Numero')</th>
                            <th>@lang('Parroquia')</th>
                            <th>@lang('Nombres')</th>
                            <th>@lang('Apellidos')</th>
                            <th>@lang('Sacerdote que firma')</th>
                            <th>@lang('Fecha de Confirmación')</th>
                            <th>@lang('Fecha de Expedición')</th>
                            <th></th>
                            @admin
                                <th></th>
                                <th></th>
                            @endadmin
                        </tr>
                        </thead>
                        <tbody id="pannel">
                            @include('back.confirmaciones.table', compact('confirms'))
                        </tbody>
                    </table>
                </div>

                <div id="pagination" class="box-footer">
                    <div class="pagination-sm no-margin pull-right">
                        {{ $confirms->links() }}
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

@endsection

@section('js')
    <script src="{{ asset('adminlte/js/back.js') }}"></script>
    <script>

        var confirmacion = (function () {

            var onReady = function () {
                $('#pannel').on('click', 'td a.btn-danger', function (event) {
                    var that = $(this)
                    event.preventDefault()
                    swal({
                        title: '@lang('Quieres eliminar esta confirmación?')',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: '@lang('Si')',
                        cancelButtonText: '@lang('No')'
                    }).then(function () {
                        back.spin()
                        $.ajax({
                            url: that.attr('href'),
                            type: 'DELETE'
                        })
                            .done(function () {
                                that.parents('tr').remove()
                                back.unSpin()
                            })
                            .fail(function () {
                                back.fail('@lang('Parece que hay un problema con el servidor...')')
                            }
                        )
                    })
                })
            }

            return {
                onReady: onReady
            }

        })()

        $(document).ready(confirmacion.onReady)

    </script>
@endsection