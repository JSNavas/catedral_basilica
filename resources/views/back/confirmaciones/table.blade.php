@foreach($confirms as $confirm)
    <tr>
        <td>{{ $confirm->libro }}</td>
        <td>{{ $confirm->folio }}</td>
        <td>{{ $confirm->numero }}</td>
        <td>{{ $confirm->parroquia }}</td>
        <td>{{ $confirm->nombres }}</td>
        <td>{{ $confirm->apellidos }}</td>
        <td>{{ $confirm->sacerdoteFirma->nombre }}</td>
        <td>{{ $confirm->fecha_confirmacion }}</td>
        <td>{{ $confirm->fecha_expedicion }}</td>
        @admin
            <td><a class="btn btn-warning btn-xs btn-block" href="{{ route('confirms.edit', [$confirm->id]) }}" role="button" title="@lang('Editar')"><span class="fa fa-edit"></span></a></td>
            <td><a class="btn btn-danger btn-xs btn-block" href="{{ route('confirms.destroy', [$confirm->id]) }}" role="button" title="@lang('Eliminar')"><span class="fa fa-remove"></span></a></td>
        @endadmin
        <td><a class="btn btn-info btn-xs btn-block" href="{{ route('confirms.export_pdf', [$confirm->id]) }}" role="button" title="@lang('PDF')"><span class="fa fa-download"></span></a></td>
    </tr>
@endforeach

