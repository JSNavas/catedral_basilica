@extends('back.confirmaciones.template')

@section('form-open')
    <form method="post" action="{{ route('confirms.update', [$confirm->id]) }}">
        {{ method_field('PUT') }}
@endsection
