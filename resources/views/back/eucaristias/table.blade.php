@foreach($eucaristias as $eucaristia)
    <tr>
        <td>{{ $eucaristia->libro }}</td>
        <td>{{ $eucaristia->folio }}</td>
        <td>{{ $eucaristia->numero }}</td>
        <td>{{ $eucaristia->parroquia }}</td>
        <td>{{ $eucaristia->nombres }}</td>
        <td>{{ $eucaristia->apellidos }}</td>
        <td>{{ $eucaristia->sacerdoteFirma->nombre }}</td>
        <td>{{ $eucaristia->fecha_nacimiento }}</td>
        <td>{{ $eucaristia->fecha_expedicion }}</td>
        <td><a class="btn btn-warning btn-xs btn-block" href="{{ route('eucaristias.edit', [$eucaristia->id]) }}" role="button" title="@lang('Editar')"><span class="fa fa-edit"></span></a></td>
        <td><a class="btn btn-danger btn-xs btn-block" href="{{ route('eucaristias.destroy', [$eucaristia->id]) }}" role="button" title="@lang('Eliminar')"><span class="fa fa-remove"></span></a></td>
    </tr>
@endforeach

