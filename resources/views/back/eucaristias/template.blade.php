@extends('back.layout')

@section('js')
    <script src="{{ asset ('adminlte/js/jquery.inputmask.js') }}"></script>
    <script src="{{ asset ('adminlte/js/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset ('adminlte/js/jquery.inputmask.extensions.js') }}"></script>

    <script>
        $(function () {
            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy/mm/dd', { 'placeholder': 'yyyy/mm/dd' })

            //Money Euro
            $('[data-mask]').inputmask()
        })
    </script>
@endsection

@section('main')

    @yield('form-open')
        {{ csrf_field() }}

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Eucaristía</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row">

                    <div class="col-md-2">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Libro',
                                'name' => 'libro',
                                'value' => isset($eucaristia) ? $eucaristia->libro : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-2">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Folio',
                                'name' => 'folio',
                                'value' => isset($eucaristia) ? $eucaristia->folio : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-2">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Número',
                                'name' => 'numero',
                                'value' => isset($eucaristia) ? $eucaristia->numero : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-3">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Parroquia',
                                'name' => 'parroquia',
                                'value' => isset($eucaristia) ? $eucaristia->parroquia : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Sacerdote Celebrante</label>
                            <select class="form-control select2" style="width: 100%;" id="sacerdote_celebrante_id" name="sacerdote_celebrante_id" required>
                                <option value>Seleccione un sacerdote</option>
                                @foreach ($sacerdotes as $sacerdote)

                                    @if(isset($eucaristia) && $eucaristia->sacerdote_celebrante_id == $sacerdote->id)
                                        <option value="{{ $sacerdote->id }}" selected>{{ $sacerdote->nombre }}</option>
                                    @else
                                        <option value="{{ $sacerdote->id }}">{{ $sacerdote->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-3">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombres',
                                'name' => 'nombres',
                                'value' => isset($eucaristia) ? $eucaristia->nombres : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-3">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Apellidos',
                                'name' => 'apellidos',
                                'value' => isset($eucaristia) ? $eucaristia->apellidos : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Sacerdote que expide original</label>
                            <select class="form-control select2" style="width: 100%;" id="sacerdote_expide_id" name="sacerdote_expide_id" required>
                                <option value>Seleccione un sacerdote</option>
                                @foreach ($sacerdotes as $sacerdote)

                                    @if(isset($eucaristia) && $eucaristia->sacerdote_expide_id == $sacerdote->id)
                                        <option value="{{ $sacerdote->id }}" selected>{{ $sacerdote->nombre }}</option>
                                    @else
                                        <option value="{{ $sacerdote->id }}">{{ $sacerdote->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Sacerdote que firma copia u original</label>
                            <select class="form-control select2" style="width: 100%;" id="sacerdote_firma_id" name="sacerdote_firma_id" required>
                                <option value>Seleccione un sacerdote</option>
                                @foreach ($sacerdotes as $sacerdote)

                                    @if(isset($eucaristia) && $eucaristia->sacerdote_firma_id == $sacerdote->id)
                                        <option value="{{ $sacerdote->id }}" selected>{{ $sacerdote->nombre }}</option>
                                    @else
                                        <option value="{{ $sacerdote->id }}">{{ $sacerdote->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>    
                </div>

                <div class="row">

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombre de la Madre',
                                'name' => 'nombre_madre',
                                'value' => isset($eucaristia) ? $eucaristia->nombre_madre : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombre del Padre',
                                'name' => 'nombre_padre',
                                'value' => isset($eucaristia) ? $eucaristia->nombre_padre : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Hijo</label>
                            <select class="form-control select2" style="width: 100%;" id="hijo" name="hijo" required>
                                <option value>Seleccione un tipo</option>

                                    @if(isset($bautizo) && $bautizo->hijo == 'Legítimo')
                                        <option value="Legítimo" selected>Legítimo</option>
                                        <option value="Natural">Natural</option>
                                        <option value="Reconocido">Reconocido</option>

                                    @elseif(isset($bautizo) && $bautizo->hijo == 'Natural')
                                        <option value="Legítimo">Legítimo</option>
                                        <option value="Natural" selected>Natural</option>
                                        <option value="Reconocido">Reconocido</option>

                                    @elseif(isset($bautizo) && $bautizo->hijo == 'Reconocido')
                                        <option value="Legítimo">Legítimo</option>
                                        <option value="Natural">Natural</option>
                                        <option value="Reconocido" selected>Reconocido</option>
                                    @else
                                        <option value="Legítimo">Legítimo</option>
                                        <option value="Natural">Natural</option>
                                        <option value="Reconocido">Reconocido</option>
                                    @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha de Nacimiento</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>

                                @if(isset($eucaristia) && $eucaristia->fecha_nacimiento)
                                    <input type="text" value="{{$eucaristia->fecha_nacimiento}}" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha de Bautismo</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>

                                @if(isset($eucaristia) && $eucaristia->fecha_bautismo)
                                    <input type="text" value="{{$eucaristia->fecha_bautismo}}" class="form-control" id="fecha_bautismo" name="fecha_bautismo" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_bautismo" name="fecha_bautismo" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                       <div class="form-group">
                            <label>Fecha de Expedición</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                                @if(isset($eucaristia) && $eucaristia->fecha_expedicion)
                                    <input type="text" value="{{$eucaristia->fecha_expedicion}}" class="form-control" id="fecha_expedicion" name="fecha_expedicion" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_expedicion" name="fecha_expedicion" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Lugar Nacimiento',
                                'name' => 'lugar_nacimiento',
                                'value' => isset($eucaristia) ? $eucaristia->lugar_nacimiento : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Padrino 1',
                                'name' => 'padrino_1',
                                'value' => isset($eucaristia) ? $eucaristia->padrino_1 : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-4">
                       @include('back.partials.input', [
                            'input' => [
                                'title' => 'Padrino 2',
                                'name' => 'padrino_2',
                                'value' => isset($eucaristia) ? $eucaristia->padrino_2 : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Notas',
                                'name' => 'notas',
                                'value' => isset($eucaristia) ? $eucaristia->notas : '',
                                'input' => 'textarea',
                                'rows' => '2',
                                'required' => false,
                            ],
                        ])
                    </div>
                </div>
            </div>      
        </div>

        <button type="submit" class="btn btn-primary">@lang('Guardar')</button>
    </form>

@endsection

@section('js')

    <script src="{{ asset('adminlte/plugins/voca/voca.min.js') }}"></script>
    <script>

        $('#slug').keyup(function () {
            $(this).val(v.slugify($(this).val()))
        })

        $('#title').keyup(function () {
            $('#slug').val(v.slugify($(this).val()))
        })

    </script>

@endsection