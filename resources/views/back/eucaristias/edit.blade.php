@extends('back.eucaristias.template')

@section('form-open')
    <form method="post" action="{{ route('eucaristias.update', [$eucaristia->id]) }}">
        {{ method_field('PUT') }}
@endsection
