<html>

<head>
    <style>
        body {
            font-family: arial;
						font-size: 20px;
        }

        @page {
            margin: 160px 50px;
        }

        header {
            position: fixed;
            left: 20px;
            top: -160px;
            right: 0px;
            height: 100px;
            background-color: #fff;
            text-align: center;
        }

        header h1 {
            margin: 10px 0;
        }

        header h2 {
            margin: 0 0 10px 0;
        }

        footer {
            position: fixed;
            left: 0px;
            bottom: -50px;
            right: 0px;
            height: 40px;
        }
        .firma {
					margin: 0px;
        }

        footer .page:after {
            content: counter(page);
        }

        footer table {
            width: 100%;
        }

        footer p {
            text-align: right;
        }

        footer .izquierda {
						position: absolute;
						top: 80px;
        }
        footer .derecha {
						position: absolute;
            top: 83px;
						right: 10px;
        }

        footer .center {
						margin-left: 65px;
            text-align: center;
        }

				.tab {
					text-indent: 2em;
					text-align: justify;
  				text-justify: inter-word;
				}
				
				.contenedor {
					line-height: 200% !important;
				}

								.align {
					line-height: 170% !important;
					text-align: center;
				}

				.subrayado {
					text-decoration: underline;
				}

				.footer {
					text-align: center;
					font-size: 14px;
					position: absolute;
					top: 80px;
				}
				.firma {
					text-align: center;
					position: absolute;
					bottom: 250px;
				}
				.centrar {
					text-align: center;
				}
    </style>

<body>
    <header>
        <br/><br/>
				<img src="{{ public_path() }}/images/logocatedral.png" height="100" width="100">
        <h1>Arquidiócesis de Coro - Venezuela</h1>
        <h2 style="font-weight: 300;">Constancia de Matrimonio Eclesiástico</h2>
    </header>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>



    <div id="content">
        <p class="tab contenedor">
			Yo, <strong class="subrayado">{{ $matrimonio->sacerdoteCelebrante->titulo.' '.$matrimonio->sacerdoteCelebrante->nombre }}</strong>, hago
			constar que el dia <strong class="subrayado">{{ Carbon\Carbon::parse($matrimonio->fecha_matrimonio)->format('d/m/Y') }}</strong> contrajeron matrimonio canónico en esta jurisdicción parroquial:
			<br/>
        	<br/>
					
			El Sr. <strong class="subrayado">{{ $matrimonio->nombres_esposo.' '.$matrimonio->apellidos_esposo }}</strong> y la Sra. <strong class="subrayado">{{ $matrimonio->nombres_esposa.' '.$matrimonio->apellidos_esposa }}</strong>
        	<br/>
			Ministro que asistió el matrimonio: <strong class="subrayado">{{ $matrimonio->sacerdoteExpide->titulo.', '.$matrimonio->sacerdoteExpide->nombre }}</strong>
			<br/>
			Iglesia donde se efectuó: <strong class="subrayado">{{ $matrimonio->parroquia }}</strong>
			<br/>
			Fueron testigos: <strong class="subrayado">{{ $matrimonio->testigos }}</strong>
			<br/>
			Es fiel copia de original, expedida en <strong>Coro, Estado Falcón, el {{ Carbon\Carbon::parse($matrimonio->fecha_expedicion)->format('d/m/Y') }}</strong><br/>
			Doy fé:
        </p>
    </div>

    <footer>
        <table>
					<tr>
							<td>
								<div class="footer">
									<hr/>
									Catedral Basílica Menor, Calle Palmasola entre calle Ciencias y Federacion,
									Frente a la plaza Bolivar. Coro. Edo. Falcón - Venezuela Teléfono de contacto +58 (0268)2515874.
									Email: Catedraldecoro@gmail.com
								</div>
							</td>
					</tr>
					<tr>

							<td>
								<div class="firma">
									<hr style="width: 250px; position: relative;top:14px;"/>
									<p class="centrar">
												{{ $matrimonio->sacerdoteFirma->nombre }}
												<br/>
												{{ $matrimonio->sacerdoteFirma->titulo }}
										</p>
								</div>
							</td>
					</tr>
        </table>
    </footer>
</body>

</html>