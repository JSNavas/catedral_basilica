@extends('back.matrimonios.template')

@section('form-open')
    <form method="post" action="{{ route('matrimonios.update', [$matrimonio->id]) }}">
        {{ method_field('PUT') }}
@endsection
