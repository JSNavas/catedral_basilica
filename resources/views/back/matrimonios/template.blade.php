@extends('back.layout')

@section('js')
    <script src="{{ asset ('adminlte/js/jquery.inputmask.js') }}"></script>
    <script src="{{ asset ('adminlte/js/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ asset ('adminlte/js/jquery.inputmask.extensions.js') }}"></script>

    <script>
        $(function () {
            //Datemask dd/mm/yyyy
            $('#datemask').inputmask('yyyy/mm/dd', { 'placeholder': 'yyyy/mm/dd' })

            //Money Euro
            $('[data-mask]').inputmask()
        })
    </script>
@endsection

@section('main')

    @yield('form-open')
        {{ csrf_field() }}

        <div class="box box-primary">

            <div class="box-body">

                <div class="row">

                    <div class="col-md-2">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Libro',
                                'name' => 'libro',
                                'value' => isset($matrimonio) ? $matrimonio->libro : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-2">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Folio',
                                'name' => 'folio',
                                'value' => isset($matrimonio) ? $matrimonio->folio : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-2">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Número',
                                'name' => 'numero',
                                'value' => isset($matrimonio) ? $matrimonio->numero : '',
                                'input' => 'number',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-3">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Parroquia',
                                'name' => 'parroquia',
                                'value' => isset($matrimonio) ? $matrimonio->parroquia : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Sacerdote Celebrante</label>
                            <select class="form-control select2" style="width: 100%;" id="sacerdote_celebrante_id" name="sacerdote_celebrante_id" required>
                                <option value>Seleccione un sacerdote</option>
                                @foreach ($sacerdotes as $sacerdote)

                                    @if(isset($matrimonio) && $matrimonio->sacerdote_celebrante_id == $sacerdote->id)
                                        <option value="{{ $sacerdote->id }}" selected>{{ $sacerdote->nombre }}</option>
                                    @else
                                        <option value="{{ $sacerdote->id }}">{{ $sacerdote->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Esposo</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row">

                    <div class="col-md-6">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombres',
                                'name' => 'nombres_esposo',
                                'value' => isset($matrimonio) ? $matrimonio->nombres_esposo : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-6">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Apellidos',
                                'name' => 'apellidos_esposo',
                                'value' => isset($matrimonio) ? $matrimonio->apellidos_esposo : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    
                </div>

                <div class="row">
                    

                    <div class="col-md-6">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombre de la Madre',
                                'name' => 'nombre_madre_esposo',
                                'value' => isset($matrimonio) ? $matrimonio->nombre_madre_esposo : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-6">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombre del Padre',
                                'name' => 'nombre_padre_esposo',
                                'value' => isset($matrimonio) ? $matrimonio->nombre_padre_esposo : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Parroquia donde fue bautizado',
                                'name' => 'parroquia_bautismo_esposo',
                                'value' => isset($matrimonio) ? $matrimonio->parroquia_bautismo_esposo : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>    
                    

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha de Bautismo</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>

                                @if(isset($matrimonio) && $matrimonio->fecha_bautismo_esposo)
                                    <input type="text" value="{{$matrimonio->fecha_bautismo_esposo}}" class="form-control" id="fecha_bautismo_esposo" name="fecha_bautismo_esposo" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_bautismo_esposo" name="fecha_bautismo_esposo" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Ciudad',
                                'name' => 'ciudad_bautismo_esposo',
                                'value' => isset($matrimonio) ? $matrimonio->ciudad_bautismo_esposo : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Esposa</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="row">

                    <div class="col-md-6">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombres',
                                'name' => 'nombres_esposa',
                                'value' => isset($matrimonio) ? $matrimonio->nombres_esposa : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-6">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Apellidos',
                                'name' => 'apellidos_esposa',
                                'value' => isset($matrimonio) ? $matrimonio->apellidos_esposa : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    
                </div>

                <div class="row">
                    

                    <div class="col-md-6">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombre de la Madre',
                                'name' => 'nombre_madre_esposa',
                                'value' => isset($matrimonio) ? $matrimonio->nombre_madre_esposa : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                    <div class="col-md-6">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Nombre del Padre',
                                'name' => 'nombre_padre_esposa',
                                'value' => isset($matrimonio) ? $matrimonio->nombre_padre_esposa : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Parroquia donde fue bautizado',
                                'name' => 'parroquia_bautismo_esposa',
                                'value' => isset($matrimonio) ? $matrimonio->parroquia_bautismo_esposa : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>        

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha de Bautismo</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>

                                @if(isset($matrimonio) && $matrimonio->fecha_bautismo_esposa)
                                    <input type="text" value="{{$matrimonio->fecha_bautismo_esposa}}" class="form-control" id="fecha_bautismo_esposa" name="fecha_bautismo_esposa" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_bautismo_esposa" name="fecha_bautismo_esposa" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Ciudad',
                                'name' => 'ciudad_bautismo_esposa',
                                'value' => isset($matrimonio) ? $matrimonio->ciudad_bautismo_esposa : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-primary">
            
            <div class="box-body">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fecha de Matrimonio</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>

                                @if(isset($matrimonio) && $matrimonio->fecha_matrimonio)
                                    <input type="text" value="{{$matrimonio->fecha_matrimonio}}" class="form-control" id="fecha_matrimonio" name="fecha_matrimonio" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_matrimonio" name="fecha_matrimonio" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fecha de Expedición</label>

                            <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>

                                @if(isset($matrimonio) && $matrimonio->fecha_expedicion)
                                    <input type="text" value="{{$matrimonio->fecha_expedicion}}" class="form-control" id="fecha_expedicion" name="fecha_expedicion" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @else
                                    <input type="text" class="form-control" id="fecha_expedicion" name="fecha_expedicion" data-inputmask="'alias': 'yyyy/mm/dd'" data-mask required>
                                @endif
                            </div>
                        </div>
                    </div>        

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Sacerdote que expide original</label>
                            <select class="form-control select2" style="width: 100%;" id="sacerdote_expide_id" name="sacerdote_expide_id" required>
                                <option value>Seleccione un sacerdote</option>
                                @foreach ($sacerdotes as $sacerdote)

                                    @if(isset($matrimonio) && $matrimonio->sacerdote_expide_id == $sacerdote->id)
                                        <option value="{{ $sacerdote->id }}" selected>{{ $sacerdote->nombre }}</option>
                                    @else
                                        <option value="{{ $sacerdote->id }}">{{ $sacerdote->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Sacerdote que firma copia u original</label>
                            <select class="form-control select2" style="width: 100%;" id="sacerdote_firma_id" name="sacerdote_firma_id" required>
                                <option value>Seleccione un sacerdote</option>
                                @foreach ($sacerdotes as $sacerdote)

                                    @if(isset($matrimonio) && $matrimonio->sacerdote_firma_id == $sacerdote->id)
                                        <option value="{{ $sacerdote->id }}" selected>{{ $sacerdote->nombre }}</option>
                                    @else
                                        <option value="{{ $sacerdote->id }}">{{ $sacerdote->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Testigos',
                                'name' => 'testigos',
                                'value' => isset($matrimonio) ? $matrimonio->testigos : '',
                                'input' => 'text',
                                'required' => true,
                            ],
                        ])
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        @include('back.partials.input', [
                            'input' => [
                                'title' => 'Notas',
                                'name' => 'notas',
                                'value' => isset($matrimonio) ? $matrimonio->notas : '',
                                'input' => 'textarea',
                                'rows' => '2',
                                'required' => false,
                            ],
                        ])
                    </div>

                </div>
            </div>      
        </div>

        <button type="submit" class="btn btn-primary">@lang('Guardar')</button>
    </form>

@endsection

@section('js')

    <script src="{{ asset('adminlte/plugins/voca/voca.min.js') }}"></script>
    <script>

        $('#slug').keyup(function () {
            $(this).val(v.slugify($(this).val()))
        })

        $('#title').keyup(function () {
            $('#slug').val(v.slugify($(this).val()))
        })

    </script>

@endsection