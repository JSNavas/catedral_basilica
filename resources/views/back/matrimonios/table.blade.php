@foreach($matrimonios as $matrimonio)
    <tr>
        <td>{{ $matrimonio->libro }}</td>
        <td>{{ $matrimonio->folio }}</td>
        <td>{{ $matrimonio->numero }}</td>
        <td>{{ $matrimonio->parroquia }}</td>
        <td>{{ $matrimonio->nombres_esposo }}</td>
        <td>{{ $matrimonio->apellidos_esposo }}</td>
        <td>{{ $matrimonio->nombres_esposa }}</td>
        <td>{{ $matrimonio->apellidos_esposa }}</td>
        <td>{{ $matrimonio->sacerdoteFirma->nombre }}</td>
        <td>{{ $matrimonio->fecha_matrimonio }}</td>
        <td>{{ $matrimonio->fecha_expedicion }}</td>
        @admin
            <td><a class="btn btn-warning btn-xs btn-block" href="{{ route('matrimonios.edit', [$matrimonio->id]) }}" role="button" title="@lang('Editar')"><span class="fa fa-edit"></span></a></td>
            <td><a class="btn btn-danger btn-xs btn-block" href="{{ route('matrimonios.destroy', [$matrimonio->id]) }}" role="button" title="@lang('Eliminar')"><span class="fa fa-remove"></span></a></td>
        @endadmin
        <td><a class="btn btn-info btn-xs btn-block" href="{{ route('matrimonios.export_pdf', [$matrimonio->id]) }}" role="button" title="@lang('PDconfirmF')"><span class="fa fa-download"></span></a></td>
    </tr>
@endforeach

