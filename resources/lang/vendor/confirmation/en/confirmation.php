<?php

return [
    'email-title' => 'Verificacion de email',
    'email-intro'=> 'Para validar su correo electrónico haga clic en el botón de abajo',
    'email-button' => 'Verificacion de email',
    'message' => 'Gracias por registrarte! Por favor revise su correo electrónico.',
    'success' => 'Has verificado correctamente tu cuenta! Ahora puede iniciar sesión.',
    'again' => 'Debe verificar su correo electrónico antes de poder acceder al sitio. ' .
                '<br>Si no ha recibido el correo electrónico de confirmación, revise su carpeta de spam.'.
                '<br>Para obtener un nuevo correo electrónico de confirmación por favor <a href="' . url('confirmation/resend') . '" class="alert-link">click aqui</a>.',
    'resend' => 'Se ha enviado un mensaje de confirmación. Por favor revise su correo electrónico.'
];
