<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Debe ser aceptado',
    'active_url'           => 'No es una URL válida.',
    'after'                => 'Debe ser una fecha después :date.',
    'after_or_equal'       => 'Debe ser una fecha después o igual a :date.',
    'alpha'                => 'Solo puede contener letras.',
    'alpha_dash'           => 'Solo puede contener letras, números y guiones.',
    'alpha_num'            => 'Solo puede contener letras y números.',
    'array'                => 'Debe ser un arreglo.',
    'before'               => 'Debe ser una fecha antes :date.',
    'before_or_equal'      => 'Debe ser una fecha antes o igual a :date.',
    'between'              => [
        'numeric' => 'Debe estar entre :min y :max.',
        'file'    => 'Debe estar entre :min y :max kilobytes.',
        'string'  => 'Debe estar entre :min y :max caracteres.',
        'array'   => 'Debe tener entre :min y :max elementos.',
    ],
    'boolean'              => 'Debe ser verdadero o falso.',
    'confirmed'            => 'La confirmación no coincide.',
    'date'                 => 'No es una fecha válida.',
    'date_format'          => 'No coincide con el formato :format.',
    'different'            => 'Este campo y :other deben ser diferente.',
    'digits'               => 'Debe ser :digits dígitos.',
    'digits_between'       => 'Debe estar entre :min and :max digits.',
    'dimensions'           => 'Tiene dimensiones de imagen inválidas.',
    'distinct'             => 'Tiene un valor duplicado.',
    'email'                => 'Debe ser una dirección de correo electrónico válida.',
    'exists'               => 'El campo seleccionado es inválido.',
    'file'                 => 'Debe ser un archivo.',
    'filled'               => 'Debe tener un valor.',
    'image'                => 'Debe ser una imagen.',
    'in'                   => 'El campo seleccionado es inválido.',
    'in_array'             => 'No existe en :other.',
    'integer'              => 'Debe ser un entero.',
    'ip'                   => 'Debe ser una dirección IP válida.',
    'ipv4'                 => 'Debe ser una dirección IPv4 válida.',
    'ipv6'                 => 'Debe ser una dirección IPv6 válida.',
    'json'                 => 'Debe ser una cadena JSON válida.',
    'max'                  => [
        'numeric' => 'No puede ser mayor que :max.',
        'file'    => 'No puede ser mayor que :max kilobytes.',
        'string'  => 'No puede ser mayor que :max caracteres.',
        'array'   => 'No puede tener más de :max elementos.',
    ],
    'mimes'                => 'Debe ser un archivo de tipo: :values.',
    'mimetypes'            => 'Debe ser un archivo de tipo: :values.',
    'min'                  => [
        'numeric' => 'Debe tener al menos :min.',
        'file'    => 'Debe tener al menos :min kilobytes.',
        'string'  => 'Debe tener al menos :min caracteres.',
        'array'   => 'Debe tener al menos :min elementos.',
    ],
    'not_in'               => 'El campo seleccionado es inválido.',
    'numeric'              => 'Debe ser un número.',
    'present'              => 'Debe ser presente.',
    'regex'                => 'tiene un formato inválido..',
    'required'             => 'Este campo es requerido.',
    'required_if'          => 'Este campo es requerido cuando :other es :value.',
    'required_unless'      => 'Este campo es requerido a no ser que :other esté en :values.',
    'required_with'        => 'Este campo es requerido cuando :values está presente.',
    'required_with_all'    => 'Este campo es requerido cuando :values está presente.',
    'required_without'     => 'Este campo es requerido cuando :values está not presente.',
    'required_without_all' => 'Este campo es requerido cuando ninguno de :values están presente.',
    'same'                 => 'Este campo y :other debe coincidir.',
    'size'                 => [
        'numeric' => 'Debe ser :size.',
        'file'    => 'Debe ser :size kilobytes.',
        'string'  => 'Debe ser :size caracteres.',
        'array'   => 'Debe contener :size elementos.',
    ],
    'string'               => 'Debe ser una cadena.',
    'timezone'             => 'Debe ser una zona valida.',
    'unique'               => 'Ya está registrado.',
    'uploaded'             => 'No se pudo cargar.',
    'url'                  => 'El formato es inválido.',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'meta_keywords' => [
            'regex' => "Las palabras clave, separadas por comas (sin espacios), deben tener un máximo de 50 caracteres.",
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
