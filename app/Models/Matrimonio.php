<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matrimonio extends Model {

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'libro', 'folio','numero', 'parroquia', 'nombres_esposo', 'apellidos_esposo', 'nombre_madre_esposo', 'nombre_padre_esposo', 'parroquia_bautismo_esposo', 'ciudad_bautismo_esposo', 'fecha_bautismo_esposo', 'nombres_esposa', 'apellidos_esposa', 'nombre_madre_esposa', 'nombre_padre_esposa', 'parroquia_bautismo_esposa', 'ciudad_bautismo_esposa', 'fecha_bautismo_esposa', 'fecha_matrimonio', 'fecha_expedicion', 'testigos', 'sacerdote_celebrante_id', 'sacerdote_expide_id', 'sacerdote_firma_id', 'notas'
    ];

	/**
	 * Get the route key for the model.
	 *
	 * @return string
	 */
	public function getRouteKeyName()
	{
	    return request()->segment(1) === 'admin' ? 'id' : 'slug';
    }

    public function scopeSearch($query, $type, $search)
	{
        if($search){
            if($type == "libro"){
                return $query->where('libro', '=', "$search");
    
            }
            if($type == "folio"){
                return $query->where('folio', '=', "$search");
    
            }
            if($type == "numero"){
                return $query->where('numero', '=', "$search");
    
            }
            if($type == "nombres"){
                return $query->where('nombres', 'LIKE', "%$search%");
    
            }
            if($type == "apellidos"){
                return $query->where('apellidos', 'LIKE', "%$search%");
    
            }
            if($type == "fecha_matrimonio"){
                return $query->where('fecha_matrimonio', 'LIKE', "%$search%");
    
            }
            if($type == "fecha_expedicion"){
                return $query->where('fecha_expedicion', 'LIKE', "%$search%");
    
            }
        }
    }
        
    public function sacerdoteCelebrante()
    {
        return $this->hasOne('App\Models\Sacerdote', 'id', 'sacerdote_celebrante_id');
    }
    
    public function sacerdoteFirma()
    {
        return $this->hasOne('App\Models\Sacerdote', 'id', 'sacerdote_firma_id');
    }
    
    public function sacerdoteExpide()
    {
        return $this->hasOne('App\Models\Sacerdote', 'id', 'sacerdote_expide_id');
    }

}
