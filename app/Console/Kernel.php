<?php

namespace App\Console;

use Illuminate\Support\Facades\Config;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use File;
use Storage;
use Log;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $host = config('database.connections.mysql.host');
        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        $database = config('database.connections.mysql.database');

        
        $schedule->exec("mysqldump -u{$username} -p{$password} -h{$host} {$database}")
        ->everyMinute()
        ->sendOutputTo('database/backup_basilica.sql')->withoutOverlapping();

        Log::info('Se realizó la copia de seguridad con exito. ');

        $schedule->call(function () {
            $filename = 'backup_basilica.sql';
            $filePath = database_path($filename);
            $fileData = File::get($filePath);

            // Se encuentra el archivo y  se usa su (ruta) para borrarlo
            $dir = '/';
            $recursive = false; // Obtener subdirectorios también
            $contents = collect(Storage::cloud()->listContents($dir, $recursive));

            $file = $contents
                ->where('type', '=', 'file')
                ->where('filename', '=', pathinfo($filename, PATHINFO_FILENAME))
                ->where('extension', '=', pathinfo($filename, PATHINFO_EXTENSION))
                ->first(); // No puede haber nombres de archivos duplicados!
            
            // Se elimina la antigua copia de seguridad
            Storage::cloud()->delete($file['path']);
            // Se sube la nueva copia de seguridad
            Storage::cloud()->put($filename, $fileData);

            Log::info('Copia de seguridad subida a Google Drive con exito. ');

        })->everyMinute();
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
