<?php

namespace App\Http\Requests;

class MatrimonioRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'libro' => 'bail|required', 
            'folio' => 'bail|required',
            'numero' => 'bail|required', 
            'parroquia' => 'bail|required', 
            'nombres_esposo' => 'bail|required', 
            'apellidos_esposo' => 'bail|required', 
            'nombre_madre_esposo' => 'bail|required', 
            'nombre_padre_esposo' => 'bail|required', 
            'parroquia_bautismo_esposo' => 'bail|required', 
            'ciudad_bautismo_esposo' => 'bail|required', 
            'fecha_bautismo_esposo' => 'bail|required', 
            'nombres_esposa' => 'bail|required', 
            'apellidos_esposa' => 'bail|required', 
            'nombre_madre_esposa' => 'bail|required', 
            'nombre_padre_esposa' => 'bail|required', 
            'parroquia_bautismo_esposa' => 'bail|required', 
            'ciudad_bautismo_esposa' => 'bail|required', 
            'fecha_bautismo_esposa' => 'bail|required', 
            'fecha_matrimonio' => 'bail|required', 
            'fecha_expedicion' => 'bail|required', 
            'testigos' => 'bail|required|max:100', 
            'sacerdote_celebrante_id' => 'bail|required', 
            'sacerdote_expide_id' => 'bail|required', 
            'sacerdote_firma_id' => 'bail|required', 
            'notas' => 'bail',
        ];
    }
}
