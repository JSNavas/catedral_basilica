<?php

namespace App\Http\Requests;

class SacerdoteRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'nombre' => 'bail|required|max:100',
            'titulo' => 'bail|required|max:50',
        ];
    }
}
