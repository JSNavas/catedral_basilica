<?php

namespace App\Http\Requests;

class DefuncRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'libro' => 'bail|required',
            'folio' => 'bail|required',
            'numero' => 'bail|required',
            'parroquia' => 'bail|required|max:255',
            'nombres' => 'bail|required|max:255',
            'apellidos' => 'bail|required|max:255',
            'edad' => 'bail|required',
            'nombre_madre' => 'bail|required|max:255',
            'nombre_padre' => 'bail|required|max:255',
            'nombre_conyuge' => 'bail|required|max:255',
            'causa_muerte' => 'bail|required',   
            'fecha_funeral' => 'bail|required',
            'fecha_defuncion' => 'bail|required',
            'fecha_expedicion' => 'bail|required',
            'sacerdote_celebrante_id' => 'bail|required',
            'sacerdote_expide_id' => 'bail|required',
            'sacerdote_firma_id' => 'bail|required',
            'notas' => 'bail',
        ];
    }
}
