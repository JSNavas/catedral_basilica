<?php

namespace App\Http\Requests;

class BautizoRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $rules = [
            'libro' => 'bail|required',
            'folio' => 'bail|required',
            'numero' => 'bail|required',
            'nombres' => 'bail|required|max:255',
            'apellidos' => 'bail|required|max:255',
            'parroquia' => 'bail|required|max:255',
            'sacerdote_celebrante_id' => 'bail|required',
            'sacerdote_expide_id' => 'bail|required',
            'hijo' => 'bail|required',
            'nombre_madre' => 'bail|required',
            'nombre_padre' => 'bail|required',
            'sacerdote_firma_id' => 'bail|required',
            'fecha_nacimiento' => 'bail|required',
            'fecha_bautismo' => 'bail|required',
            'fecha_expedicion' => 'bail|required',
            'lugar_nacimiento' => 'bail|required',
            'padrino_1' => 'bail|required|max:100',
            'padrino_2' => 'bail|max:100',
            'notas' => 'bail',
        ];
    }
}
