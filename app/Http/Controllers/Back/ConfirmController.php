<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;
use App\ {
    Http\Controllers\Controller,
    Http\Requests\ConfirmRequest,
    Models\Confirm,
    Models\Sacerdote,
};

class ConfirmController extends Controller
{
    /**
     * Display a listing of the categories.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->old('search');
        $type = $request->old('type');

        $confirms = Confirm::orderBy('id', 'DESC')
            ->search($type, $search)
            ->paginate(8);

        return view('back.confirmaciones.index', compact ('confirms', 'search', 'type'));
    }

    /**
     * Show the form for creating a new eucaristia.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sacerdotes = Sacerdote::all();
        return view('back.confirmaciones.create', compact ('sacerdotes'));
    }

    /**
     * Store a newly created eucaristia in storage.
     *
     * @param  \App\Http\Requests\ConfirmRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ConfirmRequest $request)
    {
        Confirm::create($request->all());

        return redirect(route('confirms.index'))->with('confirmacion-ok', __('La confirmación ha sido creada exitosamente.'));
    }

    /**
     * Show the form for editing the specified eucaristia.
     *
     * @param  \App\Models\Confirm  $confirm
     * @return \Illuminate\Http\Response
     */
    public function edit(Confirm $confirm)
    {
        $sacerdotes = Sacerdote::all();
        return view('back.confirmaciones.edit', compact ('confirm', 'sacerdotes'));
    }

    /**
     * Update the specified eucaristia in storage.
     *
     * @param  \App\Http\Requests\ConfirmRequest  $request
     * @param  \App\Models\Confirm  $confirm
     * @return \Illuminate\Http\Response
     */
    public function update(ConfirmRequest $request, Confirm $confirm)
    {
        $confirm->update($request->all());

        return redirect(route('confirms.index'))->with('confirmacion-ok', __('La confirmación ha sido actualizada exitosamente.'));
    }

    /**
     * Remove the specified eucaristia from storage.
     *
     * @param  \App\Models\Confirm  $confirm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Confirm $confirm)
    {
        $confirm->delete ();

        return response ()->json ();
    }

    public function export_pdf(Confirm $confirm)
    {
        $pdf = PDF::loadView('back.confirmaciones.certificado_pdf', compact('confirm'));
        return $pdf->download('confirmacion_'.Carbon::now().'.pdf');
    }
}
