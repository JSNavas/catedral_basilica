<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\ {
    Http\Controllers\Controller,
    Http\Requests\SacerdoteRequest,
    Models\Sacerdote
};

class SacerdoteController extends Controller
{
    /**
     * Display a listing of the sacerdotes.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sacerdotes = Sacerdote::paginate(8);
        
        return view('back.sacerdotes.index', compact ('sacerdotes'));
    }

    /**
     * Show the form for creating a new categorie.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.sacerdotes.create');
    }

    /**
     * Store a newly created categorie in storage.
     *
     * @param  \App\Http\Requests\SacerdoteRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SacerdoteRequest $request)
    {
        Sacerdote::create($request->all());

        return redirect(route('sacerdotes.index'))->with('sacerdote-ok', __('El sacerdote ha sido creado exitosamente.'));
    }

    /**
     * Show the form for editing the specified categorie.
     *
     * @param  \App\Models\Sacerdote  $sacerdote
     * @return \Illuminate\Http\Response
     */
    public function edit(Sacerdote $sacerdote)
    {
        return view('back.sacerdotes.edit', compact ('sacerdote'));
    }

    /**
     * Update the specified categorie in storage.
     *
     * @param  \App\Http\Requests\SacerdoteRequest  $request
     * @param  \App\Models\Sacerdote  $sacerdote
     * @return \Illuminate\Http\Response
     */
    public function update(SacerdoteRequest $request, Sacerdote $sacerdote)
    {
        $sacerdote->update($request->all());

        return redirect(route('sacerdotes.index'))->with('sacerdote-ok', __('El sacerdote ha sido actualizado exitosamente.'));
    }

    /**
     * Remove the specified categorie from storage.
     *
     * @param  \App\Models\Sacerdote  $sacerdote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sacerdote $sacerdote)
    {
        $sacerdote->delete ();

        return response ()->json ();
    }
}
