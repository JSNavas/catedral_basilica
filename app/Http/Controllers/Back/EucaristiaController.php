<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\ {
    Http\Controllers\Controller,
    Http\Requests\EucaristiaRequest,
    Models\Eucaristia,
    Models\Sacerdote,
};

class EucaristiaController extends Controller
{
    /**
     * Display a listing of the categories.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->old('search');
        $type = $request->old('type');

        $eucaristias = Eucaristia::orderBy('id', 'DESC')
            ->search($type, $search)
            ->paginate(8);

        return view('back.eucaristias.index', compact ('eucaristias', 'search', 'type'));
    }

    /**
     * Show the form for creating a new eucaristia.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sacerdotes = Sacerdote::all();
        return view('back.eucaristias.create', compact ('sacerdotes'));
    }

    /**
     * Store a newly created eucaristia in storage.
     *
     * @param  \App\Http\Requests\EucaristiaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EucaristiaRequest $request)
    {
        Eucaristia::create($request->all());

        return redirect(route('eucaristias.index'))->with('eucaristia-ok', __('La eucaristía ha sido creada exitosamente.'));
    }

    /**
     * Show the form for editing the specified eucaristia.
     *
     * @param  \App\Models\Eucaristia  $confirmacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Eucaristia $eucaristia)
    {
        $sacerdotes = Sacerdote::all();
        return view('back.eucaristias.edit', compact ('eucaristia', 'sacerdotes'));
    }

    /**
     * Update the specified eucaristia in storage.
     *
     * @param  \App\Http\Requests\EucaristiaRequest  $request
     * @param  \App\Models\Eucaristia  $eucaristia
     * @return \Illuminate\Http\Response
     */
    public function update(EucaristiaRequest $request, Eucaristia $eucaristia)
    {
        $eucaristia->update($request->all());

        return redirect(route('eucaristias.index'))->with('eucaristia-ok', __('La eucaristía ha sido actualizada exitosamente.'));
    }

    /**
     * Remove the specified eucaristia from storage.
     *
     * @param  \App\Models\Eucaristia  $eucaristia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Eucaristia $eucaristia)
    {
        $eucaristia->delete ();

        return response ()->json ();
    }
}
