<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;
use App\ {
    Http\Controllers\Controller,
    Http\Requests\MatrimonioRequest,
    Models\Matrimonio,
    Models\Sacerdote,
};

class MatrimonioController extends Controller
{
    /**
     * Display a listing of the categories.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->old('search');
        $type = $request->old('type');

        $matrimonios = Matrimonio::orderBy('id', 'DESC')
            ->search($type, $search)
            ->paginate(8);

        return view('back.matrimonios.index', compact ('matrimonios', 'search', 'type'));
    }

    /**
     * Show the form for creating a new matrimonio.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sacerdotes = Sacerdote::all();
        return view('back.matrimonios.create', compact ('sacerdotes'));
    }

    /**
     * Store a newly created matrimonio in storage.
     *
     * @param  \App\Http\Requests\MatrimonioRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatrimonioRequest $request)
    {
        Matrimonio::create($request->all());

        return redirect(route('matrimonios.index'))->with('matrimonio-ok', __('El matrimonio ha sido creado exitosamente.'));
    }

    /**
     * Show the form for editing the specified matrimonio.
     *
     * @param  \App\Models\Matrimonio  $matrimonio
     * @return \Illuminate\Http\Response
     */
    public function edit(Matrimonio $matrimonio)
    {
        $sacerdotes = Sacerdote::all();
        return view('back.matrimonios.edit', compact ('matrimonio', 'sacerdotes'));
    }

    /**
     * Update the specified matrimonio in storage.
     *
     * @param  \App\Http\Requests\MatrimonioRequest  $request
     * @param  \App\Models\Matrimonio  $matrimonio
     * @return \Illuminate\Http\Response
     */
    public function update(MatrimonioRequest $request, Matrimonio $matrimonio)
    {
        $matrimonio->update($request->all());

        return redirect(route('matrimonios.index'))->with('matrimonio-ok', __('El matrimonio ha sido actualizado exitosamente.'));
    }

    /**
     * Remove the specified matrimonio from storage.
     *
     * @param  \App\Models\Matrimonio  $matrimonio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matrimonio $matrimonio)
    {
        $matrimonio->delete ();

        return response ()->json ();
    }

    public function export_pdf(Matrimonio $matrimonio)
    {
        $pdf = PDF::loadView('back.matrimonios.certificado_pdf', compact('matrimonio'));
        return $pdf->download('matrimonio_'.Carbon::now().'.pdf');
    }
}
