<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;
use App\ {
    Http\Controllers\Controller,
    Http\Requests\BautizoRequest,
    Models\Bautizo,
    Models\Sacerdote,
};


class BautizoController extends Controller
{
    /**
     * Display a listing of the bautizos.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->old('search');
        $type = $request->old('type');
        
        $bautizos = Bautizo::orderBy('id', 'DESC')
            ->search($type, $search)
            ->paginate(8);

        return view('back.bautizos.index', compact ('bautizos', 'search', 'type'));
    }

    /**
     * Show the form for creating a new bautizo.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sacerdotes = Sacerdote::all();
        return view('back.bautizos.create', compact ('sacerdotes'));
    }

    /**
     * Store a newly created bautizo in storage.
     *
     * @param  \App\Http\Requests\BautizoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BautizoRequest $request)
    {
        Bautizo::create($request->all());

        return redirect(route('bautizos.index'))->with('bautizo-ok', __('El bautizo ha sido creado exitosamente.'));
    }

    /**
     * Show the form for editing the specified bautizo.
     *
     * @param  \App\Models\Bautizo  $bautizo
     * @return \Illuminate\Http\Response
     */
    public function edit(Bautizo $bautizo)
    {
        $sacerdotes = Sacerdote::all();
        return view('back.bautizos.edit', compact ('bautizo', 'sacerdotes'));
    }

    /**
     * Update the specified bautizo in storage.
     *
     * @param  \App\Http\Requests\BautizoRequest  $request
     * @param  \App\Models\Bautizo  $bautizo
     * @return \Illuminate\Http\Response
     */
    public function update(BautizoRequest $request, Bautizo $bautizo)
    {
        $bautizo->update($request->all());

        return redirect(route('bautizos.index'))->with('bautizo-ok', __('El bautizo ha sido actualizado exitosamente.'));
    }

    /**
     * Remove the specified bautizo from storage.
     *
     * @param  \App\Models\bautizo  $bautizo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bautizo $bautizo)
    {
        $bautizo->delete ();

        return response ()->json ();
    }

    public function export_pdf(Bautizo $bautizo)
    {
        $pdf = PDF::loadView('back.bautizos.certificado_pdf', compact('bautizo'));
        return $pdf->download('bautizo_'.Carbon::now().'.pdf');
    }
}
