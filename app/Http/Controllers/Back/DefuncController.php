<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use PDF;
use Carbon\Carbon;
use App\ {
    Http\Controllers\Controller,
    Http\Requests\DefuncRequest,
    Models\Defunc,
    Models\Sacerdote,
};


class DefuncController extends Controller
{
    /**
     * Display a listing of the defuncs.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->old('search');
        $type = $request->old('type');

        $defuncs = Defunc::orderBy('id', 'DESC')
            ->search($type, $search)
            ->paginate(8);

        return view('back.defuncs.index', compact ('defuncs', 'search', 'type'));
    }

    /**
     * Show the form for creating a new defunc.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sacerdotes = Sacerdote::all();
        return view('back.defuncs.create', compact ('sacerdotes'));
    }

    /**
     * Store a newly created defunc in storage.
     *
     * @param  \App\Http\Requests\DefuncRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DefuncRequest $request)
    {
        Defunc::create($request->all());

        return redirect(route('defuncs.index'))->with('defuncion-ok', __('La defunción ha sido creado exitosamente.'));
    }

    /**
     * Show the form for editing the specified defunc.
     *
     * @param  \App\Models\Defunc  $defunc
     * @return \Illuminate\Http\Response
     */
    public function edit(Defunc $defunc)
    {
        $sacerdotes = Sacerdote::all();
        return view('back.defuncs.edit', compact ('defunc', 'sacerdotes'));
    }

    /**
     * Update the specified defunc in storage.
     *
     * @param  \App\Http\Requests\DefuncRequest  $request
     * @param  \App\Models\Defunc  $defunc
     * @return \Illuminate\Http\Response
     */
    public function update(DefuncRequest $request, Defunc $defunc)
    {
        $defunc->update($request->all());

        return redirect(route('defuncs.index'))->with('defuncion-ok', __('La defunción ha sido actualizado exitosamente.'));
    }

    /**
     * Remove the specified defunc from storage.
     *
     * @param  \App\Models\Defunc  $defunc
     * @return \Illuminate\Http\Response
     */
    public function destroy(Defunc $defunc)
    {
        $defunc->delete ();

        return response ()->json ();
    }

    public function export_pdf(Defunc $defunc)
    {
        $pdf = PDF::loadView('back.defuncs.certificado_pdf', compact('defunc'));
        return $pdf->download('defuncion_'.Carbon::now().'.pdf');
    }
}
