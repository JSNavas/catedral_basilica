## Catedral Basilica ##

**Catedral Basilica Web** It is a web application to manage the processes of this church.

### Installation ###

* execute `composer install`
* execute `composer update`
* copy *.env.example* to *.env*
* execute `php artisan key:generate`to generate secure key in *.env* file
* if you use MySQL in *.env* file :
   * set DB_CONNECTION
   * set DB_DATABASE
   * set DB_USERNAME
   * set DB_PASSWORD

* execute `php artisan vendor:publish --provider="Bestmomo\LaravelEmailConfirmation\ServiceProvider" --tag="confirmation:migrations"` to publish email confirmation migration
* execute `php artisan migrate:fresh && php artisan db:seed`

* edit *.env* for emails configuration

* Add to the server cron to run at 12 a.m. every day  :
   * `00 00 * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1`

* if you want to use it manually, execute :
   * `php artisan schedule:run ` to create the file with the backup located in `database/backup_basilica.sql`


### Gestion ###

To use application the database is seeding with users :

* Administrator : email = admin@gmail.com, password = admin
* Redactor : email = redactor@gmail.com, password = redac
* User : email = user@gmail.com, password = user

### Tests ###

When you want to start the tests, update and fill the database and clear the cache :

`php artisan migrate:fresh && php artisan db:seed && php artisan config:cache`


### License ###

This example for Laravel is open-sourced software licensed under the MIT license
